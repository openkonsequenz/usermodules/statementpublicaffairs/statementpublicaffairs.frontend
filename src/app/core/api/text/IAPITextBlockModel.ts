/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIRequireRuleModel} from "./IAPIRequireRuleModel";

/**
 * Interface which represents a single text block template.
 */
export interface IAPITextBlockModel {

    /**
     * Unique ID of the text block item.
     */
    id: string;

    /**
     * Template text which is placed in the statement.
     */
    text: string;

    /**
     * List of IDs which are excluded by the text block template.
     */
    excludes: string[];

    /**
     * List of rules to specify all required text block templates.
     */
    requires: IAPIRequireRuleModel[];

}

export type TAPITextBlockRuleKey = keyof IAPITextBlockModel & ("requires" | "excludes");
