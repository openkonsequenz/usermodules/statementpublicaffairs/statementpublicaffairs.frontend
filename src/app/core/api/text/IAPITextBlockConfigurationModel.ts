/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPITextBlockGroupModel} from "./IAPITextBlockGroupModel";

/**
 * Interface which represents the configuration of all text block templates.
 */
export interface IAPITextBlockConfigurationModel {

    /**
     * Object which specifies the available select options in the text block template.
     */
    selects: {
        [key: string]: string[];
    };

    /**
     * List of all available text block template groups.
     */
    groups: IAPITextBlockGroupModel[];

    /**
     * List of all available text block template groups for a negative response.
     */
    negativeGroups: IAPITextBlockGroupModel[];

}
