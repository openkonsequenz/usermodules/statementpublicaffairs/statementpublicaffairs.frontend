/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export enum EAPIUserRoles {
    DIVISION_MEMBER = "ROLE_SPA_DIVISION_MEMBER",
    ROLE_SPA_ACCESS = "ROLE_SPA_ACCESS",
    SPA_APPROVER = "ROLE_SPA_APPROVER",
    SPA_OFFICIAL_IN_CHARGE = "ROLE_SPA_OFFICIAL_IN_CHARGE",
    SPA_ADMIN = "ROLE_SPA_ADMIN",
    SPA_CUSTOMER = "ROLE_SPA_CUSTOMER"
}

export const ALL_NON_TRIVIAL_USER_ROLES = [
    EAPIUserRoles.DIVISION_MEMBER,
    EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE,
    EAPIUserRoles.SPA_APPROVER,
    EAPIUserRoles.SPA_ADMIN
];


