/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClient} from "@angular/common/http";
import {Inject, Injectable} from "@angular/core";
import {urlJoin} from "../../../util";
import {SPA_BACKEND_ROUTE} from "../../external-routes";
import {IAPIUserInfo} from "./IAPIUserInfo";
import {IAPIVersion} from "./IAPIVersion";

@Injectable({
    providedIn: "root"
})
export class CoreApiService {

    public constructor(
        protected readonly httpClient: HttpClient,
        @Inject(SPA_BACKEND_ROUTE) protected readonly baseUrl: string
    ) {

    }

    /**
     * Fetches the version info from back end.
     */
    public getVersion() {
        const endPoint = "version";
        return this.httpClient.get<IAPIVersion>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Fetches the user information
     */
    public getUserInfo() {
        const endPoint = "userinfo";
        return this.httpClient.get<IAPIUserInfo>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Extends the current session for the user.
     * This end point should be called every minute to keep the user logged in.
     */
    public keepAlive() {
        const endPoint = "keepalive-session";
        return this.httpClient.get<null>(urlJoin(this.baseUrl, endPoint));
    }

    /**
     * Invalidates the user's access token.
     */
    public logOut() {
        const endPoint = "logout";
        return this.httpClient.get<null>(urlJoin(this.baseUrl, endPoint));
    }

}
