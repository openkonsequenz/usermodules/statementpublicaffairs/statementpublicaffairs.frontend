/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPITextblockHistoryVersionContentModel} from "./IAPITextblockHistoryVersionContentModel";

export interface IStoreTextblockHistoryVersionModel extends IAPITextblockHistoryVersionModel {
    id: string;
}

export interface IAPITextblockHistoryVersionModel {

    version: string;

    timestamp: string;

    user: string;

    firstName: string;

    lastName: string;

    msg: string;

    content: IAPITextblockHistoryVersionContentModel[];

}
