/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Inject, Injectable} from "@angular/core";
import {WINDOW} from "../dom";

@Injectable({providedIn: "root"})
export class ConfirmService {

    public constructor(
        @Inject(WINDOW) private readonly window: Window
    ) {

    }

    /**
     * Opens a confirmation dialog on the browser window and returns result. OK = true, Cancel = false.
     */
    public askForConfirmation(message: string) {
        return this.window.confirm(message);
    }

}
