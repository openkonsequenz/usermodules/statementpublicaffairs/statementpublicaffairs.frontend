/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAttachmentControlValue} from "../store/attachments/model";
import {createFileMock} from "./create-file-mock.spec";

export function createAttachmentFileMock(name: string): IAttachmentControlValue {
    return {
        name,
        file: createFileMock(name),
        tagIds: []
    };
}
