/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIStatementTextConfigurationModel, IAPITextBlockGroupModel, IAPITextBlockModel} from "../core/api/text";

export const blockTexts: string[] = [
    "Lorem ipsum dolor sit amet, <f:name> \n",
    "consetetur sadipscing elitr, sed diam nonumy eirmod <d:datum> tempor invidunt ut labore et dolore magna aliquyam erat <s:options>, sed diam voluptua. \n\nAt vero eos et accusam et justo duo dolores et ea rebum.",
    `Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.
At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. \nLorem ipsum dolor sit amet, consetetur sadipscing elitr,
sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.`,
    "At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, <t:Ansprech_Partner>, <t:Firma> no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
    "Stet clita kasd gubergren, <f:Bearbeiter> \nno \n \n \nsea takimata sanctus est Lorem ipsum dolor sit amet. <t:Firma>."
];

export function createTextBlockModelMock(id: string, text: string): IAPITextBlockModel {
    return {
        id,
        requires: [],
        excludes: [],
        text
    };
}

export function createTextBlockModelGroup(id: number, size: number): IAPITextBlockGroupModel {
    return {
        groupName: "Textbausteingruppe " + id,
        textBlocks: Array(size).fill(0).map((_, i) => createTextBlockModelMock("Textbaustein " + id + "." + i, blockTexts[i]))
    };
}

export function createStatementTextConfigurationMock(): IAPIStatementTextConfigurationModel {
    return {
        configuration: {
            selects: {
                DropDown: ["Option 1", "Option 2", "Option 3"],
                options: ["null", "eins", "zwei", "drei", "siebenundzwanzig"]
            },
            groups: Array(3).fill(0)
                .map((_, i) => createTextBlockModelGroup(i, 5)),
            negativeGroups: Array(3).fill(0)
                .map((_, i) => createTextBlockModelGroup(-i, 5))
        },

        replacements: {
            name: "Hugo Haferkamp",
            test: "TEST REPLACEMENT",
            Ansprechpartner: "Donald Duck",
            Firma: "Quak GmbH",
            Ansprech_Partner: "Donald_Duck"
        }

    };
}
