/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {boolean, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {CollapsibleModule} from "./collapsible.module";

storiesOf("Shared/Layout", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({imports: [CollapsibleModule]}))
    .add("CollapsibleComponent", () => ({
        template: `
            <div style="padding: 1em; height: 100%; width: 100%; box-sizing: border-box;">
                <app-collapsible
                    style="margin: 1em; max-height: calc(100% - 2em);"
                    [appTitle]="appTitle"
                    [appHeaderTemplateRef]="showHeaderTemplate ? header : null">
                    <ng-template #header>
                        Header
                    </ng-template>
                    <div style="width: 100%; height: 20em; background: radial-gradient(rgba(0, 0, 0, 0.5), transparent)">
                    </div>
                </app-collapsible>
            </div>
        `,
        props: {
            appTitle: text("appTitle", "Title"),
            showHeaderTemplate: boolean("header", false)
        }
    }));


