/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {action} from "@storybook/addon-actions";
import {boolean, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {SearchbarModule} from "./searchbar.module";

storiesOf("Shared / Layout", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            SearchbarModule
        ]
    }))
    .add("SearchbarComponent", () => ({
        template: `
            <div style="padding: 1em;">
                <app-searchbar style="width: 100%;"
                    [appIsLoading]="appIsLoading"
                    [appPlaceholder]="appPlaceholder"
                    [appSearchText]="appSearchText"
                    (appSearch)="appSearch($event)">
                </app-searchbar>
            </div>
        `,
        props: {
            appIsLoading: boolean("appIsLoading", false),
            appPlaceholder: text("appPlaceholder", ""),
            appSearchText: text("appSearchText", ""),
            appSearch: action("appSearch")
        }
    }));
