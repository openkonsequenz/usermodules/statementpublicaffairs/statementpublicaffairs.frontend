/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
    selector: "app-action-button",
    templateUrl: "./action-button.component.html",
    styleUrls: ["./action-button.component.scss"]
})
export class ActionButtonComponent {

    @Input()
    public appDisabled: boolean;

    @Input()
    public appIcon: string;

    @Input()
    public appType: string;

    @Input()
    public appRouterLink: string;

    @Input()
    public appStatementId: number;

    @Input()
    public appMailId: string;

    @Input()
    public appNewTab: boolean;

    @Output()
    public appClick = new EventEmitter<MouseEvent>();

}
