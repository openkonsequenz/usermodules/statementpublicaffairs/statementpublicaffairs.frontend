/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {action} from "@storybook/addon-actions";
import {boolean, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {FileDropModule} from "../file-drop.module";

storiesOf("Shared/Layout", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({imports: [FileDropModule]}))
    .add("FileDropComponent", () => ({
        template: `
            <app-file-drop
                #fileDrop
                style="margin: 1em;"
                [appDisabled]="appDisabled"
                (appFileDrop)="appFileDrop($event)"
                [appPlaceholder]="appPlaceholder">
                <div style="margin: auto" *ngIf="content">
                    {{content}}
                </div>
            </app-file-drop>
            <div>
                <button (click)="fileDrop.openDialog();" class="openk-button" style="margin: 0 1em;">
                    Open Dialog
                </button>
            </div>
        `,
        props: {
            content: text("Content", undefined),
            appPlaceholder: text("appPlaceholder", "Drop files here."),
            appDisabled: boolean("appDisabled", false),
            appFileDrop: action("appFileDrop")
        }
    }));
