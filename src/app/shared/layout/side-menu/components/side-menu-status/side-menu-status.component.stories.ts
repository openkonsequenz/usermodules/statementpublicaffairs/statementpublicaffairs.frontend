/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {boolean, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {SideMenuModule} from "../../side-menu.module";

storiesOf("Shared / Layout / Side Menu", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            SideMenuModule
        ]
    }))
    .add("SideMenuStatusComponent", () => ({
        template: `
            <app-side-menu-status style="max-width: 15em; margin: 1em;"
                [appLoading]="appLoading"
                [appLoadingMessage]="appLoadingMessage">
                {{content}}
            </app-side-menu-status>
        `,
        props: {
            appLoading: boolean("appLoading", false),
            appLoadingMessage: text("appLoadingMessage", "Loading..."),
            content: text("content", "Content...")
        }
    }));
