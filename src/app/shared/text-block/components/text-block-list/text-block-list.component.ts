/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CdkDrag, CdkDropList} from "@angular/cdk/drag-drop";
import {DOCUMENT} from "@angular/common";
import {Component, EventEmitter, Inject, Input, Output} from "@angular/core";
import {IAPITextBlockModel} from "../../../../core";

@Component({
    selector: "app-text-block-list",
    templateUrl: "./text-block-list.component.html",
    styleUrls: ["./text-block-list.component.scss"]
})
export class TextBlocksListComponent {

    @Input()
    public appConnectedTo: CdkDropList | string | Array<CdkDropList | string>;

    @Input()
    public appDisabled: boolean;

    @Input()
    public appDuplicateOnDrag: boolean;

    @Input()
    public appEnterPredicate: (drag: CdkDrag, drop: CdkDropList) => boolean = returnFalse;

    @Input()
    public appForAdmin: boolean;

    @Input()
    public appHiddenIds: string[];

    @Input()
    public appListData: IAPITextBlockModel[];

    @Input()
    public appReplacements: { [key: string]: string };

    @Input()
    public appSelectedIds: string[];

    @Input()
    public appShortMode: boolean;

    @Input()
    public appShowPreview: boolean;

    @Input()
    public appWithoutTitlePrefix: boolean;

    @Output()
    public appAdd = new EventEmitter<{ textBlock: IAPITextBlockModel; index: number }>();

    @Output()
    public appDelete = new EventEmitter<{ textBlock: IAPITextBlockModel; index: number }>();

    @Output()
    public appEdit = new EventEmitter<{ textBlock: IAPITextBlockModel; index: number }>();

    @Output()
    public appUp = new EventEmitter<{ textBlock: IAPITextBlockModel; index: number }>();

    @Output()
    public appDown = new EventEmitter<{ textBlock: IAPITextBlockModel; index: number }>();

    public isDragging: boolean;

    public constructor(@Inject(DOCUMENT) public document: Document) {

    }

    public trackBy(index) {
        return index;
    }

    public startDrag(block: IAPITextBlockModel) {
        if (this.appDuplicateOnDrag && Array.isArray(this.appListData)) {
            const index = this.appListData.indexOf(block);
            this.appListData.splice(index + 1, 0, {...block});
        }
    }

    public stopDrag(block: IAPITextBlockModel) {
        if (this.appDuplicateOnDrag && Array.isArray(this.appListData)) {
            const index = this.appListData.lastIndexOf(block);
            this.appListData.splice(index, 1);
        }
    }

}

function returnFalse() {
    return false;
}
