/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CdkDragDrop, CdkDropList} from "@angular/cdk/drag-drop";
import {Component, EventEmitter, Input, Output, ViewChild} from "@angular/core";
import {IAPITextBlockGroupModel} from "../../../../core";
import {IExtendedTextBlockModel} from "../../model";

/**
 * This component shows two lists of text blocks that can be selected, by drag and drop or pressing a button.
 * First part are the standard blocks that are always available (newline, freetext, pagebreak). They can be placed multiple times so they
 * stay in the list after selecting one. The other part are the text blocks, provided in groups. A textblock can only be placed once, so it
 * is removed from the list after being selected once.
 */
@Component({
    selector: "app-text-block-select",
    templateUrl: "./text-block-select.component.html",
    styleUrls: ["./text-block-select.component.scss"]
})
export class TextBlockSelectComponent {

    @Input()
    public appShortMode: boolean;

    @Input()
    public appGroups: IAPITextBlockGroupModel[];

    @Input()
    public appSelectedIds: string[];

    @Input()
    public appConnectedTo: CdkDropList | string | Array<CdkDropList | string>;

    @Input()
    public appDisabled: boolean;

    @Input()
    public appReplacements: { [key: string]: string };

    @Output()
    public appAdd = new EventEmitter<IExtendedTextBlockModel>();

    @Output()
    public appRemove = new EventEmitter<number>();

    @ViewChild(CdkDropList, {static: true})
    public dropList: CdkDropList;

    /**
     * Block models for the always available blocks. They are not part of the backend textblockconfig so they need to be defined here.
     */
    public standardBlocks: IExtendedTextBlockModel[] = [
        {id: "Freitext", text: "", excludes: [], requires: [], type: "text"},
        {id: "Zeilenumbruch", text: "", excludes: [], requires: [], type: "newline"},
        {id: "Seitenumbruch", text: "", excludes: [], requires: [], type: "pagebreak"}
    ];

    public drop(event: CdkDragDrop<IExtendedTextBlockModel>) {
        if (event.container !== event.previousContainer) {
            this.appRemove.emit(event.previousIndex);
        }
    }

}
