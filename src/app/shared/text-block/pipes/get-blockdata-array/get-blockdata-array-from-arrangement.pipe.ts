/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IAPITextArrangementItemModel, IAPITextBlockModel} from "../../../../core/api/text";
import {ITextBlockRenderItem} from "../../model/ITextBlockRenderItem";

import {alwaysReplace, replaceTags, textToBlockDataArray} from "./block-data-helper";
import {IReplacement} from "./IReplacement";

/**
 * Takes in a IAPITextArrangementItemModel and splits it into an array of ITextBlockRenderItem.
 * Text, new lined and the replacements are separated. The replacements like freetext, date and select are grouped together as one type,
 * text-fill. That is done to be able to mark them in the ui.
 */
@Pipe({
    name: "getBlockDataFromArrangement"
})
export class GetBlockDataFromArrangementPipe implements PipeTransform {

    public transform(textArrangement: IAPITextArrangementItemModel,
                     blockModel: IAPITextBlockModel,
                     placeHolderValues: { [key: string]: string },
                     replacementTexts: { [key: string]: string },
                     selects: { [key: string]: string[] }): ITextBlockRenderItem[] {

        if (textArrangement?.replacement != null) {
            const whatToReplace: IReplacement[] = [...alwaysReplace];
            const blockData: ITextBlockRenderItem[] = [{value: textArrangement.replacement, type: "text"}];
            return replaceTags(blockData, whatToReplace, placeHolderValues, replacementTexts, selects);
        }
        if (textArrangement?.type === "pagebreak" || textArrangement?.type === "newline") {
            return [];
        }

        return textToBlockDataArray(blockModel, placeHolderValues, replacementTexts, selects);
    }
}
