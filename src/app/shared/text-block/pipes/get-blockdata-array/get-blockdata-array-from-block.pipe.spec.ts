/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPITextBlockModel} from "../../../../core/api/text";
import {ITextBlockRenderItem} from "../../model/ITextBlockRenderItem";
import {GetBlockDataFromBlockModelPipe} from "./get-blockdata-array-from-block.pipe";


describe("GetBlockDataFromBlockModelPipe", () => {

    const pipe = new GetBlockDataFromBlockModelPipe();

    const blockModel: IAPITextBlockModel = {
        id: "textblockId",
        text: "this is a test <f:freetext><t:replacementtext><s:select><d:date> and more text afterwards",
        excludes: [],
        requires: []
    };

    it("should return empty array for missing blockmodel (undefined/null)", () => {
        let result = pipe.transform(undefined);
        expect(result).toEqual([]);

        result = pipe.transform(null);
        expect(result).toEqual([]);
    });

    it("should split up textblockmodel text content into the different types and return them as array", () => {

        const expectedResult: ITextBlockRenderItem[] = [
            {
                type: "text",
                value: "this is a test "
            },
            {
                type: "highlight-text",
                value: "freetext",
                iconType: "input"
            },
            {
                type: "text-fill",
                value: "replacementtext"
            },
            {
                type: "highlight-text",
                value: "select",
                iconType: "select"
            },
            {
                type: "highlight-text",
                value: "date",
                iconType: "date"
            },
            {
                type: "text",
                value: " and more text afterwards"
            }
        ];

        const result = pipe.transform(blockModel);
        expect(JSON.parse(JSON.stringify(result))).toEqual(expectedResult);
    });

});
