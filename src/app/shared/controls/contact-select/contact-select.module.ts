/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {TranslateModule} from "@ngx-translate/core";
import {ContactTableModule} from "../../layout/contact-table";
import {PaginationCounterModule} from "../../layout/pagination-counter";

import {SearchbarModule} from "../../layout/searchbar";
import {ContactSelectComponent} from "./contact-select.component";

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        PaginationCounterModule,
        SearchbarModule,
        ContactTableModule
    ],
    declarations: [ContactSelectComponent],
    exports: [ContactSelectComponent]
})
export class ContactSelectModule {

}
