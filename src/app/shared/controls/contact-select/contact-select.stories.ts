/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {number, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {timer} from "rxjs";
import {IAPIContactPerson} from "../../../core/api/contacts/IAPIContactPerson";
import {IAPIContactPersonDetails} from "../../../core/api/contacts/IAPIContactPersonDetails";
import {I18nModule} from "../../../core/i18n";
import {ContactSelectModule} from "./contact-select.module";

const appContacts: IAPIContactPerson[] = new Array(20).fill(0).map(() => (
    {
        firstName: "Vorname",
        lastName: "Nachname",
        email: "test@email.com",
        companyName: "Straßenbau Quak GmbH",
        companyId: "1",
        id: "1"
    })
);

const appDetails: IAPIContactPersonDetails = {
    community: "Entenhausen",
    communitySuffix: "",
    company: "Straßenbau Quak GmbH",
    email: "dagobert.duck@quak.de",
    firstName: "Dagobert",
    houseNumber: "19",
    lastName: "Duck",
    postCode: "98765",
    salutation: "",
    street: "An der Schnabelweide",
    title: ""
};

let detailsToShow = false;

const toggleDetails = (id: number) => {
    detailsToShow = !!id;
};

const details: () => IAPIContactPersonDetails = () => detailsToShow ? appDetails : null;

let isLoading = false;

let searchText = "";

const search = async (text: string) => {
    searchText = text;
    isLoading = true;

    await timer(2000).toPromise();
    if (searchText === text) {
        isLoading = false;
    }
};

const searchIsLoading: () => boolean = () => isLoading;

storiesOf("Features / 05 Contacts", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({imports: [ContactSelectModule, I18nModule]}))
    .add("ContactSelectComponent", () => ({
        template: `
            <div style="padding: 1em;">
                <app-contact-select
                    (appSearchChange)="search($event)"
                    [appIsLoading]="searchIsLoading()"
                    [appPage]="currentPage"
                    [appPageSize]="maxPages"
                    [appEntries]="appContacts.slice(0, numberOfRows)"
                    [appMessage]="'contacts.selectContact' | translate"
                    [appDetails]="details()"
                    (appValueChange)="toggleDetails($event)">
                </app-contact-select>
            </div>
        `,
        props: {
            currentPage: number("current page", 1, {min: 1, max: 10}),
            maxPages: number("max pages", 1, {min: 1, max: 10}),
            appContacts,
            numberOfRows: number("number of rows", 11, {min: 0, max: 15}),
            toggleDetails,
            details,
            search,
            searchIsLoading
        }
    }));
