/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {I18nModule} from "../../../../core";
import {StatementSelectModule} from "../statement-select.module";
import {StatementSelectComponent} from "./statement-select.component";

describe("StatementSelectComponent", () => {
    let component: StatementSelectComponent;
    let fixture: ComponentFixture<StatementSelectComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                StatementSelectModule,
                I18nModule,
                RouterTestingModule
            ]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementSelectComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should toggle statements", () => {
        component.appValue = [19];

        component.toggle(1919, true);
        expect(component.appValue).toEqual([19, 1919]);

        component.toggle(1919, false);
        expect(component.appValue).toEqual([19]);
    });

});
