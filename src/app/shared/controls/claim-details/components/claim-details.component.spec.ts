/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {SimpleChanges} from "@angular/core";
import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {APP_CONFIGURATION, I18nModule} from "../../../../core";
import {IAPIClaimDetails} from "../../../../core/api/process/IAPIClaimDetails";
import {ClaimDetailsComponent} from "./claim-details.component";

describe("ClaimDetailsComponent", () => {
    let component: ClaimDetailsComponent;
    let fixture: ComponentFixture<ClaimDetailsComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [ClaimDetailsComponent],
            imports: [I18nModule],
            providers: [{
                provide: APP_CONFIGURATION, useValue: {
                    claimDetails: {
                        displayClaimDetails: false,
                        pollClaimDetails: false,
                        thresholdToShowWarningInMinutes: 5
                    }
                }
            }]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ClaimDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should calculate timeLeft when claimDetails change", () => {
        component.claimDetails = {
            claimedUntilTime: "Thu Jan 01 1970 01:02:05 GMT+0100 (Mitteleuropäische Normalzeit)",
            currentTime: "Thu Jan 01 1970 01:00:05 GMT+0100 (Mitteleuropäische Normalzeit)"
        } as IAPIClaimDetails;

        component.ngOnChanges({claimDetails: {}} as unknown as SimpleChanges);

        expect(component.timeLeftInMinutes).toEqual(2);

        component.timeLeftInMinutes = 0;
        fixture.detectChanges();
        component.ngOnChanges({} as unknown as SimpleChanges);
        expect(component.timeLeftInMinutes).toEqual(0);

        component.claimDetails = null;
        fixture.detectChanges();
        component.ngOnChanges({claimDetails: {}} as unknown as SimpleChanges);
        expect(component.timeLeftInMinutes).toEqual(2);
    });

});
