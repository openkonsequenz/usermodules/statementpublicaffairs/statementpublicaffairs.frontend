/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, forwardRef, Inject, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from "@angular/core";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {interval, Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";
import {APP_CONFIGURATION, IAppConfiguration} from "../../../../core";
import {IAPIClaimDetails} from "../../../../core/api/process/IAPIClaimDetails";

@Component({
    selector: "app-claim-details",
    templateUrl: "./claim-details.component.html",
    styleUrls: ["./claim-details.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ClaimDetailsComponent),
            multi: true
        }
    ]
})
export class ClaimDetailsComponent implements OnDestroy, OnInit, OnChanges {

    @Input()
    public claimDetails: IAPIClaimDetails;

    public timeLeftInMinutes: number;

    public showWarningThreshold = 5;

    private endTime: number;

    private destroy$ = new Subject<void>();

    public constructor(@Inject(APP_CONFIGURATION) public configuration: IAppConfiguration) {
        if (configuration?.claimDetails?.thresholdToShowWarningInMinutes != null) {
            this.showWarningThreshold = configuration.claimDetails.thresholdToShowWarningInMinutes;
        }
    }

    public ngOnInit() {
        interval(1000).pipe(
            takeUntil(this.destroy$)
        ).subscribe(() => {
            this.calculateTimeLeft();
        });
    }

    public ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public ngOnChanges(changes: SimpleChanges) {
        if (changes.claimDetails) {
            this.calculateEndTime();
            this.calculateTimeLeft();
        }
    }

    private calculateTimeLeft(): void {
        const timestamp = Date.now();

        this.timeLeftInMinutes = Math.max(Math.ceil((this.endTime - timestamp) / 60000), 0);
    }

    private calculateEndTime(): void {
        if (this.claimDetails == null) {
            return;
        }
        const {claimedUntilTime, currentTime} = this.claimDetails;
        const timestamp = Date.now();

        const timeLeft = Date.parse(claimedUntilTime) - Date.parse(currentTime);

        this.endTime = (new Date(timestamp + timeLeft)).getTime();
    }

}
