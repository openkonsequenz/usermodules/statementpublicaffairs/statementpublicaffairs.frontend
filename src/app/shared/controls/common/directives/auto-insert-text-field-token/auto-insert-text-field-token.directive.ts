/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Directive, ElementRef} from "@angular/core";

@Directive({
    selector: "textarea [appAutoInsertTextFieldToken]",
    exportAs: "appAutoInsertTextFieldToken"
})
export class AutoInsertTextFieldTokenDirective {

    public constructor(public elementRef: ElementRef<HTMLTextAreaElement>) {

    }

    public insert(token: string, startToken?: string, requireLineBreak?: boolean) {
        const withStartToken = startToken != null;
        startToken = withStartToken ? startToken : "";
        const selectionStart = this.elementRef.nativeElement.selectionStart;
        const selectionEnd = this.elementRef.nativeElement.selectionEnd;
        const value = this.elementRef.nativeElement.value;
        const valueFirstPart = value.slice(0, selectionStart);
        if (requireLineBreak && withStartToken && valueFirstPart.length > 0 && !valueFirstPart.endsWith("\n")) {
            startToken = "\n" + startToken;
        }

        this.setValue(""
            + valueFirstPart
            + startToken
            + value.slice(selectionStart, selectionEnd)
            + token
            + value.slice(selectionEnd)
        );

        this.setCursor(selectionEnd + startToken.length + (withStartToken ? 0 : token.length));
    }

    public setValue(value: string) {
        this.elementRef.nativeElement.value = value;
        this.elementRef.nativeElement.dispatchEvent(new InputEvent("input", {
            inputType: "insertText",
            data: value
        }));
    }

    public setCursor(position: number) {
        this.elementRef.nativeElement.selectionStart = position;
        this.elementRef.nativeElement.selectionEnd = position;
        this.elementRef.nativeElement.focus();
    }

}
