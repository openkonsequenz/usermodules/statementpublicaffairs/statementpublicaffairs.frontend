/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {CalendarModule} from "primeng/calendar";
import {DropDownModule} from "../../layout/drop-down";
import {DateControlComponent} from "./component/date-control.component";
import {MomentPipe} from "./pipe/moment.pipe";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        CalendarModule,
        DropDownModule,
        MatIconModule
    ],
    declarations: [
        DateControlComponent,
        MomentPipe
    ],
    exports: [
        DateControlComponent,
        MomentPipe
    ]
})
export class DateControlModule {

}
