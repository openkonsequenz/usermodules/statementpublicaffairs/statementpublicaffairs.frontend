/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {action} from "@storybook/addon-actions";
import {boolean, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {DateControlModule} from "../date-control.module";

storiesOf("Shared/Controls", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({imports: [DateControlModule, BrowserAnimationsModule]}))
    .add("DateControlComponent", () => ({
        template: `
            <app-date-control
                style="margin: 1em;"
                [appDisabled]="appDisabled"
                [appDisplayFormat]="appDisplayFormat"
                [appValue]="appValue"
                [appInfo]="appInfo"
                [appSuccess]="appSuccess"
                [appWarning]="appWarning"
                [appDanger]="appDanger"
                (appValueChange)="appValueChange($event)">
            </app-date-control>
        `,
        props: {
            appDisabled: boolean("appDisabled", false),
            appDisplayFormat: text("appDisplayFormat", "DD.MM.YYYY"),
            appValue: text("appValue", "2020-05-19"),
            appInfo: boolean("appInfo", false),
            appSuccess: boolean("appSuccess", false),
            appWarning: boolean("appWarning", false),
            appDanger: boolean("appDanger", false),
            appValueChange: action("appValueChange")
        }
    }));


