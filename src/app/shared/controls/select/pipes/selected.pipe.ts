/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {ISelectOption} from "../model";

@Pipe({name: "selected"})
export class SelectedPipe implements PipeTransform {


    /**
     * Returns the first option object in the array which has the given value.
     * @param options List of options in which the result is searched
     * @param selectedValue Value for which the options are searched for
     * @param autoSelectFirst If true and if selectedValue is null, the first entry options is returned
     */
    public transform<T = any>(options: ISelectOption<T>[], selectedValue: T, autoSelectFirst?: boolean): ISelectOption<T> {
        if (!Array.isArray(options)) {
            return undefined;
        }

        if (selectedValue == null && autoSelectFirst) {
            return options[0];
        }

        if (selectedValue == null) {
            return undefined;
        }

        return options.find((o) => o?.value === selectedValue);
    }

}
