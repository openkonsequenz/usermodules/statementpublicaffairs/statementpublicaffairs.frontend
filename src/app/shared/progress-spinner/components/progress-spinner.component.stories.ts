/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {ProgressSpinnerModule} from "../progress-spinner.module";

storiesOf("Shared", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({imports: [ProgressSpinnerModule]}))
    .add("ProgressSpinnerComponent", () => ({
        template: `
            <app-progress-spinner style="margin: 1em;">
            </app-progress-spinner>
        `
    }));
