/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {AbstractControl} from "@angular/forms";

@Pipe({name: "getFormError", pure: false})
export class GetFormErrorPipe implements PipeTransform {

    public transform(control: AbstractControl, errorCode: string, ...path: Array<string | number>): any {
        return control?.getError(errorCode, path);
    }

}
