/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {PairPipe} from "./pair.pipe";

describe("PairPipe", () => {

    it("should convert the input array to an array of pairs", () => {
        const pairPipe = new PairPipe();
        expect(pairPipe.transform(undefined)).toEqual([]);
        expect(pairPipe.transform(null)).toEqual([]);
        expect(pairPipe.transform([undefined, null, undefined, null, null])).toEqual([]);
        expect(pairPipe.transform([0, 1, 2, 3, 4, 5, 6])).toEqual([[0, 1], [2, 3], [4, 5], [6]]);
        expect(pairPipe.transform([0, 1, 2, 3, 4, 5, 6], 2)).toEqual([[0, 1], [2, 3], [4, 5], [6]]);
        expect(pairPipe.transform([0, 1, 2, 3, 4, 5, 6], 3)).toEqual([[0, 1, 2], [3, 4, 5], [6]]);
    });

});
