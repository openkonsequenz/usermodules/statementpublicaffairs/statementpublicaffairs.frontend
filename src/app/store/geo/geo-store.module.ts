/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {EffectsModule} from "@ngrx/effects";
import {StoreModule} from "@ngrx/store";
import {NominatimSearchEffect, OpenGisEffect} from "./effects";
import {GEO_NAME, GEO_REDUCERS} from "./geo-reducers.token";

@NgModule({
    imports: [
        StoreModule.forFeature(GEO_NAME, GEO_REDUCERS),
        EffectsModule.forFeature([
            OpenGisEffect,
            NominatimSearchEffect
        ])
    ]
})
export class GeoStoreModule {

}
