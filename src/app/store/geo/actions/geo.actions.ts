/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";
import {IAPINominatimSearchResult} from "../../../core";
import {ILeafletBounds} from "../../../features/map";

export const openGisAction = createAction(
    "[Map] Open GIS",
    props<{ bounds: ILeafletBounds; user: string }>()
);

export const searchMapAction = createAction(
    "[Map] Search for a place to display on map",
    props<{ q: string }>()
);

export const setSearchResponseAction = createAction(
    "[Store] Sets the search result",
    props<{ response: IAPINominatimSearchResult[] }>()
);

export const setMapSearchLoadingAction = createAction(
    "[Store] Sets the search loading state",
    props<{ loading: boolean }>()
);
