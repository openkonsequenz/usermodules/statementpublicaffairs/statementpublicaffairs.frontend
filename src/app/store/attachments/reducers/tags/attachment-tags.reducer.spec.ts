/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Action} from "@ngrx/store";
import {AUTO_SELECTED_TAGS, IAPIAttachmentTag} from "../../../../core/api/attachments";
import {setAttachmentTagsAction} from "../../actions";
import {attachmentTagsReducer} from "./attachment-tags.reducer";

describe("attachmentTagsReducer", () => {

    const initialState: IAPIAttachmentTag[] = [];

    it("should set a list of attachment tags", () => {
        const tags: IAPIAttachmentTag[] = AUTO_SELECTED_TAGS.map((_) => ({id: _, label: _}));
        const action: Action = setAttachmentTagsAction({tags});
        const state = attachmentTagsReducer(initialState, action);
        expect(state).toEqual(tags);
    });

});
