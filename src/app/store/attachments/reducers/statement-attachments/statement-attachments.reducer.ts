/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {arrayJoin, filterDistinctValues, setEntitiesObject, TStoreEntities} from "../../../../util/store";
import {addAttachmentEntityAction, deleteAttachmentsAction, setAttachmentsAction} from "../../actions";

export const statementAttachmentsReducer = createReducer<TStoreEntities<number[]>>(
    {},
    on(addAttachmentEntityAction, (state, action) => {
        const statementId = action.statementId;
        const entityIds = filterDistinctValues(arrayJoin(state[statementId], [action.entity?.id]));
        return setEntitiesObject(state, [entityIds], () => statementId);
    }),
    on(setAttachmentsAction, (state, action) => {
        const statementId = action.statementId;
        const entityIds = filterDistinctValues(arrayJoin(action.entities).map((_) => _?.id));

        return setEntitiesObject(state, [entityIds], () => statementId);
    }),
    on(deleteAttachmentsAction, (state, action) => {
        const statementId = action.statementId;
        const entityIdsToDelete = arrayJoin(action.entityIds);
        const entityIds = arrayJoin(state[statementId]).filter((_) => entityIdsToDelete.indexOf(_) === -1);

        return setEntitiesObject(state, [entityIds], () => statementId);
    })
);
