/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";
import {IAPIAttachmentModel, IAPIAttachmentTag} from "../../../core/api";
import {IAttachmentControlValue, IAttachmentFormValue} from "../model";

export const fetchAttachmentsAction = createAction(
    "[Details/Edit] Fetch attachments",
    props<{ statementId: number }>()
);

export const fetchAttachmentTagsAction = createAction(
    "[Details/Edit] Fetch attachments"
);

export const setAttachmentTagsAction = createAction(
    "[API] Set attachment tags",
    props<{ tags: IAPIAttachmentTag[] }>()
);

export const submitAttachmentFormAction = createAction(
    "[Details/Edit] Add or remove attachments",
    props<{
        statementId: number;
        taskId: string;
        value?: IAttachmentFormValue;
    }>()
);

export const setAttachmentsAction = createAction(
    "[API] Set attachments",
    props<{ statementId: number; entities: IAPIAttachmentModel[] }>()
);

export const addAttachmentEntityAction = createAction(
    "[API] Add attachment",
    props<{ statementId: number; entity: IAPIAttachmentModel }>()
);

export const deleteAttachmentsAction = createAction(
    "[API] Delete attachments",
    props<{ statementId: number; entityIds: number[] }>()
);


export const updateAttachmentTagsAction = createAction(
    "[Edit] Update attachment tags",
    props<{ items: IAttachmentControlValue[] }>()
);

export const setAttachmentCacheAction = createAction(
    "[Edit] Set attachment cache",
    props<{ statementId: number; items: IAttachmentControlValue[] }>()
);

export const clearAttachmentCacheAction = createAction(
    "[Edit] Clear attachment cache",
    props<{ statementId: number }>()
);


export const startAttachmentDownloadAction = createAction(
    "[Edit/Details] Start attachment download",
    props<{ statementId: number; attachmentId: number }>()
);
