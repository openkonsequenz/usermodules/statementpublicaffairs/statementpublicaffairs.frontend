/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {IAPIPaginationResponse} from "../../../../core";
import {setStatementSearchResultAction} from "../../actions";

export const statementSearchReducer = createReducer<IAPIPaginationResponse<number>>(
    undefined,
    on(setStatementSearchResultAction, (state, action) => {
        if (action.results == null) {
            return undefined;
        }
        return {
            ...action.results,
            content: (Array.isArray(action.results.content) ? action.results.content : [])
                .filter((statement) => statement?.id != null)
                .map((statement) => statement.id)
        };
    })
);
