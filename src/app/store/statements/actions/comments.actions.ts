/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createAction, props} from "@ngrx/store";

export const fetchCommentsAction = createAction(
    "[Details/Edit] Fetch all comments to statement",
    props<{ statementId: number }>()
);

export const addCommentAction = createAction(
    "[Details/Edit] Add comment to statement",
    props<{ statementId: number; text: string }>()
);

export const deleteCommentAction = createAction(
    "[Details/Edit] Delete comment from statement",
    props<{ statementId: number; commentId: number }>()
);

export const editCommentAction = createAction(
    "[Details/Edit] Edits the comment with the new text value",
    props<{ statementId: number; commentId: number; newText: string }>()
);
