/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {FormArray, FormControl} from "@angular/forms";
import {IDepartmentOptionValue} from "..";
import {IAPITextArrangementItemModel} from "../../../../core";
import {createFormGroup} from "../../../../util/forms";
import {createAttachmentForm, IAttachmentFormValue} from "../../../attachments/model";

export interface IStatementEditorFormValue {

    arrangement: IAPITextArrangementItemModel[];

    attachments?: IAttachmentFormValue;

    contributions: { selected: IDepartmentOptionValue[]; indeterminate: IDepartmentOptionValue[] };

}

export function createStatementEditorForm() {
    return createFormGroup<IStatementEditorFormValue>({
        arrangement: new FormArray([]),
        attachments: createAttachmentForm(),
        contributions: new FormControl({selected: [], indeterminate: []})
    });
}
