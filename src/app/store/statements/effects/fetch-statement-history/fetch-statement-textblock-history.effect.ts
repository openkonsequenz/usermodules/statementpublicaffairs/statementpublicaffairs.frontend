/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {merge, Observable} from "rxjs";
import {endWith, filter, map, retry, startWith, switchMap} from "rxjs/operators";
import {ContactsApiService, SettingsApiService, StatementsApiService} from "../../../../core";
import {catchErrorTo} from "../../../../util";
import {EErrorCode, setErrorAction} from "../../../root";
import {setStatementTypesAction} from "../../../settings";
import {fetchStatementTextblockHistoryAction, setStatementLoadingAction, updateStatementEntityAction} from "../../actions";

@Injectable({providedIn: "root"})
export class FetchStatementTextblockHistoryEffect {

    public readonly fetchStatementTextblockHistory$ = createEffect(() => this.actions.pipe(
        ofType(fetchStatementTextblockHistoryAction),
        filter((action) => action.statementId != null),
        switchMap((action) => this.fetchStatementTextblockHistory(action.statementId).pipe(
            startWith(setStatementLoadingAction({loading: {fetchingStatementDetails: true}})),
            endWith(setStatementLoadingAction({loading: {fetchingStatementDetails: false}})),
        ))
    ));

    public constructor(
        private readonly actions: Actions,
        private readonly statementsApiService: StatementsApiService,
        private readonly contactsApiService: ContactsApiService,
        private readonly settingsApiService: SettingsApiService
    ) {

    }

    public fetchStatementTextblockHistory(statementId: number): Observable<Action> {
        return this.statementsApiService.getTextblockHistory(statementId).pipe(
            retry(2),
            switchMap((history) => merge(
                this.fetchStatementInformation(statementId),
                this.fetchStatementContact(statementId),
                this.fetchStatementTypes()
            ).pipe(
                startWith(updateStatementEntityAction({statementId, entity: {history}}))
            )),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
        );
    }

    public fetchStatementInformation(statementId: number) {
        return this.statementsApiService.getStatement(statementId).pipe(
            retry(2),
            map((info) => updateStatementEntityAction({statementId, entity: {info}})),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
        );
    }

    public fetchStatementContact(statementId: number) {
        return this.contactsApiService.getStatementsContact(statementId).pipe(
            map((contactInfo) => updateStatementEntityAction({statementId, entity: {contactInfo}})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
        );
    }

    public fetchStatementTypes(): Observable<Action> {
        return this.settingsApiService.getStatementTypes().pipe(
            map((statementTypes) => setStatementTypesAction({statementTypes})),
            retry(2),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED}))
        );
    }

}
