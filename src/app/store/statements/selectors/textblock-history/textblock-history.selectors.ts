/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createSelector} from "@ngrx/store";
import {ISelectOption} from "../../../../shared/controls/select";
import {entitiesToArrayWithId, selectPropertyProjector} from "../../../../util";
import {statementSelector} from "../statement.selectors";

export const statementTextblockHistorySelector = createSelector(
    statementSelector,
    selectPropertyProjector("history")
);

export const statementTextblockHistoryVersionsSelector = createSelector(
    statementTextblockHistorySelector,
    (historyState) => entitiesToArrayWithId(historyState?.versions)
        .map<ISelectOption<string>>((version, index, array) =>
        ({label: index < array.length - 1 ? version?.id : version?.id + " (aktuell)", value: version?.id}))
);
