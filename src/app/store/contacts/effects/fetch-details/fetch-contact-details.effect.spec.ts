/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {Observable, of, Subscription} from "rxjs";
import {SPA_BACKEND_ROUTE} from "../../../../core";
import {IAPIContactPersonDetails} from "../../../../core/api/contacts/IAPIContactPersonDetails";
import {fetchContactDetailsAction, setContactEntityAction, setContactsLoadingState} from "../../actions";
import {FetchContactDetailsEffect} from "./fetch-contact-details.effect";

describe("FetchContactDetailsEffect", () => {

    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: FetchContactDetailsEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                FetchContactDetailsEffect,
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(FetchContactDetailsEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should fetch contact details and dispatch setContactEntityAction", () => {
        const contactId = "contactId";

        const fetchResult: IAPIContactPersonDetails =
            {
                community: "string",
                communitySuffix: "string",
                company: "string",
                email: "string",
                firstName: "string",
                houseNumber: "string",
                lastName: "string",
                postCode: "string",
                salutation: "string",
                street: "string",
                title: "string"
            };

        const expectedResult = [
            setContactsLoadingState({state: {fetching: true}}),
            setContactEntityAction({entity: {...fetchResult, id: contactId}}),
            setContactsLoadingState({state: {fetching: false}})
        ];
        const results: Action[] = [];

        actions$ = of(fetchContactDetailsAction({contactId, statementId: undefined}));
        subscription = effect.fetch$.subscribe((action) => results.push(action));

        expectFetchContactDetails(contactId, fetchResult);
        expect(results).toEqual(expectedResult);
        httpTestingController.verify();
    });

    function expectFetchContactDetails(contactId: string, returnValue: IAPIContactPersonDetails) {
        const url = `/contacts/${contactId}`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("GET");
        request.flush(returnValue);
    }

});

