/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {setContactsLoadingState} from "../../actions";
import {IContactsLoadingState} from "../../model";
import {contactsLoadingReducer} from "./contacts-loading.reducer";

describe("contactsLoadingReducer", () => {

    it("should override loading/fetching in the state", () => {
        let initialState: IContactsLoadingState = {searching: true, fetching: false};
        let action = setContactsLoadingState({state: {}});
        let state = contactsLoadingReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setContactsLoadingState({state: {searching: false}});
        state = contactsLoadingReducer(initialState, action);
        expect(state).toEqual({searching: false, fetching: false});

        action = setContactsLoadingState({state: {fetching: null}});
        state = contactsLoadingReducer(initialState, action);
        expect(state).toEqual({searching: true, fetching: null});

        initialState = {};
        action = setContactsLoadingState({state: {searching: false, fetching: true}});
        state = contactsLoadingReducer(initialState, action);
        expect(state).toEqual({searching: false, fetching: true});
    });
});
