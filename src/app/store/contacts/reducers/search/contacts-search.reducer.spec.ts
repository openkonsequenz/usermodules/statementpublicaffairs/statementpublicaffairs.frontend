/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIPaginationResponse} from "../../../../core";
import {setContactsSearchAction} from "../../actions";
import {IContactEntity} from "../../model";
import {contactsSearchReducer} from "./contacts-search.reducer";


describe("contactsSearchReducer", () => {

    it("should set the list of ids from the response to the state content", () => {
        const initialState: IAPIPaginationResponse<string> = undefined;
        let action = setContactsSearchAction(undefined);
        let state = contactsSearchReducer(initialState, action);
        expect(state).toEqual(initialState);

        action = setContactsSearchAction({results: null});
        state = contactsSearchReducer(initialState, action);
        expect(state).toEqual(initialState);

        let response = {
            content: [
                {
                    firstName: "firstName"
                },
                {
                    firstName: "firstName"
                }
            ]
        } as IAPIPaginationResponse<Partial<IContactEntity>>;
        action = setContactsSearchAction({results: response});
        state = contactsSearchReducer(initialState, action);
        let expectedState = {content: []} as IAPIPaginationResponse<string>;
        expect(state).toEqual(expectedState);

        response = {
            content: [
                {
                    id: "1",
                    firstName: "firstName"
                },
                {
                    id: "2",
                    lastName: "lastName"
                }
            ]
        } as IAPIPaginationResponse<Partial<IContactEntity>>;
        action = setContactsSearchAction({results: response});
        state = contactsSearchReducer(initialState, action);
        expectedState = {content: ["1", "2"]} as IAPIPaginationResponse<string>;
        expect(state).toEqual(expectedState);
    });
});




