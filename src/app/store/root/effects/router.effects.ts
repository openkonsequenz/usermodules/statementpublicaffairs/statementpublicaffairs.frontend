/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Actions, createEffect} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {setQueryParamsAction} from "../actions";

@Injectable({providedIn: "root"})
export class RouterEffects {

    public readonly queryParams$ = createEffect(() => this.extractQueryParams());

    public constructor(
        private readonly actions: Actions,
        private readonly activatedRoute: ActivatedRoute
    ) {

    }

    private extractQueryParams(): Observable<Action> {
        return this.activatedRoute.queryParams.pipe(
            map((queryParams) => setQueryParamsAction({queryParams}))
        );
    }

}
