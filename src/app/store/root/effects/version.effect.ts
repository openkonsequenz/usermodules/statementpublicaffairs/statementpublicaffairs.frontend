/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {Observable} from "rxjs";
import {map, switchMap} from "rxjs/operators";
import {CoreApiService} from "../../../core";
import {retryAfter} from "../../../util";
import {fetchVersionAction, setBackEndVersionAction} from "../actions";

@Injectable({providedIn: "root"})
export class VersionEffect {

    public fetchVersionInfo$ = createEffect(() => this.actions$.pipe(
        ofType(fetchVersionAction),
        switchMap(() => this.fetchVersionInfo())
    ));

    public constructor(
        private readonly actions$: Actions,
        private readonly coreApiService: CoreApiService
    ) {

    }

    public fetchVersionInfo(): Observable<Action> {
        return this.coreApiService.getVersion().pipe(
            map((version) => setBackEndVersionAction(version)),
            // This call should always be successful. If not, it will be repeated every 30s until success.
            retryAfter(30000)
        );
    }

}


