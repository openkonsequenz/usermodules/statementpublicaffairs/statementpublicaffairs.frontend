/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createReducer, on} from "@ngrx/store";
import {setBackEndVersionAction} from "../actions";

export const versionBackEndReducer = createReducer<string>(
    undefined,
    on(setBackEndVersionAction, (state, payload) => payload == null ? undefined : payload.buildVersion)
);
