/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {createSelector} from "@ngrx/store";
import {rootStateSelector} from "./root.selectors";

export const versionSelector = createSelector(
    rootStateSelector,
    (state) => state.version
);

export const versionBackEndSelector = createSelector(
    rootStateSelector,
    (state) => state.versionBackEnd
);

export const overallVersionSelector = createSelector(
    versionSelector,
    versionBackEndSelector,
    (version, backEndVersion) => backEndVersion == null ? version : version + "/" + backEndVersion
);
