/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Params} from "@angular/router";
import {createSelector} from "@ngrx/store";
import {rootStateSelector} from "./root.selectors";

function selectQueryParamString(key: string) {
    return (state: Params): string => state == null ? undefined : state[key];
}

function selectQueryParamInt(key: string) {
    return (state: Params): number => {
        if (state == null || state[key] == null) {
            return undefined;
        }
        const result = typeof state[key] === "string" ? parseInt(state[key], 10) : state[key];
        return Number.isInteger(result) ? result : undefined;
    };
}


export const queryParamsSelector = createSelector(
    rootStateSelector,
    (state) => state.queryParams
);

export const queryParamsIdSelector = createSelector(
    queryParamsSelector,
    selectQueryParamInt("id")
);

export const queryParamsTaskIdSelector = createSelector(
    queryParamsSelector,
    selectQueryParamString("taskId")
);

export const queryParamsMailIdSelector = createSelector(
    queryParamsSelector,
    selectQueryParamString("mailId")
);

export const queryParamsCoordSelector = createSelector(
    queryParamsSelector,
    selectQueryParamString("coord")
);
