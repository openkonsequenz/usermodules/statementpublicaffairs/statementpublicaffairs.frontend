/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {InjectionToken} from "@angular/core";
import {ActionReducerMap} from "@ngrx/store";
import {IMailStoreState} from "./model";
import {mailEntitiesReducer, mailInboxReducer, mailLoadingReducer} from "./reducers";

export const EMAIL_NAME = "email";

export const EMAIL_REDCUERS = new InjectionToken<ActionReducerMap<IMailStoreState>>("Email store reducer", {
    providedIn: "root",
    factory: () => ({
        entities: mailEntitiesReducer,
        inboxIds: mailInboxReducer,
        loading: mailLoadingReducer
    })
});
