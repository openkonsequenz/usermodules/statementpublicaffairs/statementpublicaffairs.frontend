/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {EMPTY, Observable, Subject, Subscription} from "rxjs";
import {AuthService, SPA_BACKEND_ROUTE} from "../../../../core";
import {downloadEmailAttachmentAction} from "../../actions";
import {DownloadEmailAttachmentEffect} from "./download-email-attachment.effect";

describe("DownloadEmailAttachmentEffect", () => {

    const token = "<TOKEN>";
    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: DownloadEmailAttachmentEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                },
                {
                    provide: AuthService,
                    useValue: {token}
                }
            ]
        });
        effect = TestBed.inject(DownloadEmailAttachmentEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should download email attachment on downloadEmailAttachmentAction", () => {
        const mailId = "<Mail19>";
        const name = "attachment.pdf";
        const results: Action[] = [];
        const spy = spyOn(effect, "download").and.returnValue(EMPTY);
        const actionSubject = new Subject<Action>();
        actions$ = actionSubject;
        subscription = effect.download$.subscribe((_) => results.push(_));

        actionSubject.next(downloadEmailAttachmentAction({mailId, name}));
        expect(spy).toHaveBeenCalledWith(mailId, name);
        spy.calls.reset();

        actionSubject.next(downloadEmailAttachmentAction({mailId: null, name}));
        expect(spy).not.toHaveBeenCalled();

        actionSubject.next(downloadEmailAttachmentAction({mailId, name: null}));
        expect(spy).not.toHaveBeenCalled();

        expect(results).toEqual([]);
    });

    it("should download email attachments", () => {
        const mailId = "<Mail19>";
        const name = "attachment.pdf";
        const encodedName = encodeURI(name);
        const results: Action[] = [];
        const spy = spyOn(effect.downloadService, "startDownload");

        subscription = effect.download(mailId, name).subscribe((_) => results.push(_));

        expect(subscription.closed).toBeTrue();
        expect(spy).toHaveBeenCalledWith(`/mail/identifier/${mailId}/${encodedName}`, token);
        expect(results).toEqual([]);
    });

});

