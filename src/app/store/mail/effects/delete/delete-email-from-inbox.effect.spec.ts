/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {ActivatedRoute, Params} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {EMPTY, Observable, Subject, Subscription} from "rxjs";
import {SPA_BACKEND_ROUTE} from "../../../../core";
import {deleteEmailEntityAction, deleteEmailFromInboxAction, setEmailLoadingStateAction} from "../../actions";
import {DeleteEmailFromInboxEffect} from "./delete-email-from-inbox.effect";

describe("DeleteEmailFromInboxEffect", () => {

    let actions$: Observable<Action>;
    let queryParams: Params;
    let httpTestingController: HttpTestingController;
    let effect: DeleteEmailFromInboxEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule,
                RouterTestingModule
            ],
            providers: [
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                },
                {
                    provide: ActivatedRoute,
                    useValue: {snapshot: {queryParams: {}}}
                }
            ]
        });
        effect = TestBed.inject(DeleteEmailFromInboxEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
        queryParams = TestBed.inject(ActivatedRoute).snapshot.queryParams;
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should delete emails on deleteEmailFromInboxAction", () => {
        const mailId = "<Mail19>";
        const results: Action[] = [];
        const spy = spyOn(effect, "delete").and.returnValue(EMPTY);
        const actionSubject = new Subject<Action>();
        actions$ = actionSubject;
        subscription = effect.delete$.subscribe((_) => results.push(_));

        actionSubject.next(deleteEmailFromInboxAction({mailId}));
        expect(spy).toHaveBeenCalledWith(mailId);
        spy.calls.reset();

        actionSubject.next(deleteEmailFromInboxAction({mailId: null}));
        expect(spy).not.toHaveBeenCalled();

        expect(results).toEqual([]);
    });

    it("should delete emails from inbox", async () => {
        const mailId = "<Mail19>";
        const results: Action[] = [];

        queryParams.mailId = mailId;
        const navigationPromise = Promise.resolve(true);
        const navigateSpy = spyOn(effect.router, "navigate").and.returnValue(navigationPromise);
        navigateSpy.calls.reset();
        subscription = effect.delete(mailId).subscribe((_) => results.push(_));

        expectDeleteMailFromInboxRequest(mailId);
        await navigationPromise; // Wait for navigation to finish

        expect(subscription.closed).toBeTrue();
        expect(navigateSpy).toHaveBeenCalled();
        expect(results).toEqual([
            setEmailLoadingStateAction({loading: {deleting: true}}),
            deleteEmailEntityAction({mailId}),
            setEmailLoadingStateAction({loading: {deleting: false}})
        ]);
    });

    function expectDeleteMailFromInboxRequest(mailId: string) {
        const url = `/mail/inbox/${mailId}`;
        const request = httpTestingController.expectOne(url);
        expect(request.request.method).toBe("DELETE");
        request.flush(200);
    }

});

