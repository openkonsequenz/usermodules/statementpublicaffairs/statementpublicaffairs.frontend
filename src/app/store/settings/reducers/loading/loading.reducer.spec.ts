/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {setSettingsLoadingStateAction} from "../../actions";
import {settingsLoadingReducer} from "./loading.reducer";

describe("settingsLoadingReducer", () => {

    it("should update state on setSettingsLoadingStateAction", () => {
        const action = setSettingsLoadingStateAction({state: {fetchingDepartments: false}});
        expect(settingsLoadingReducer({fetchingDepartments: true}, action)).toEqual({fetchingDepartments: false});
    });

});
