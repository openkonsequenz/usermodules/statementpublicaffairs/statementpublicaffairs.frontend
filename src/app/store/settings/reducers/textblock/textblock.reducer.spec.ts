/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPITextBlockConfigurationModel} from "../../../../core";
import {setTextblockSettingsAction} from "../../actions";
import {textblockSettingsReducer} from "./textblock.reducer";

describe("textblockSettingsReducer", () => {

    it("should set state on setTextblockSettingsAction", () => {
        const data: IAPITextBlockConfigurationModel = {} as IAPITextBlockConfigurationModel;
        expect(textblockSettingsReducer(null, setTextblockSettingsAction({data}))).toBe(data);
    });

});
