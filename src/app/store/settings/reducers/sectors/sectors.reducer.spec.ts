/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPISectorsModel} from "../../../../core";
import {setSectorsAction} from "../../actions";
import {sectorsReducer} from "./sectors.reducer";

describe("sectorsReducer", () => {

    it("should set the sectors object as state if provided", () => {
        const initialState: IAPISectorsModel = undefined;
        let action = setSectorsAction(undefined);
        let state = sectorsReducer(initialState, action);
        expect(state).toEqual({});

        const sectors: IAPISectorsModel = {
            "Ort#Ortsteil": [
                "Strom", "Gas", "Beleuchtung"
            ]
        };
        action = setSectorsAction({sectors});
        state = sectorsReducer(initialState, action);
        expect(state).toEqual(sectors);
    });

});
