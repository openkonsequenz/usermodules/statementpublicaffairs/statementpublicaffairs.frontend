/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {TestBed} from "@angular/core/testing";
import {provideMockActions} from "@ngrx/effects/testing";
import {Action} from "@ngrx/store";
import {EMPTY, Observable, of, Subscription} from "rxjs";
import {take} from "rxjs/operators";
import {SPA_BACKEND_ROUTE} from "../../../../core";
import {submitTypesAction} from "../../../statements";
import {SubmitTypesEffect} from "./submit-types.effect";

describe("SubmitTypesEffect", () => {

    let actions$: Observable<Action>;
    let httpTestingController: HttpTestingController;
    let effect: SubmitTypesEffect;
    let subscription: Subscription;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            imports: [
                HttpClientTestingModule
            ],
            providers: [
                SubmitTypesEffect,
                provideMockActions(() => actions$),
                {
                    provide: SPA_BACKEND_ROUTE,
                    useValue: "/"
                }
            ]
        });
        effect = TestBed.inject(SubmitTypesEffect);
        httpTestingController = TestBed.inject(HttpTestingController);
    });

    afterEach(() => {
        if (subscription != null) {
            subscription.unsubscribe();
        }
    });

    it("should call submit method on submit action", () => {
        const submitSpy = spyOn(effect, "submit").and.returnValue(EMPTY);
        const types = [{label: "label"}];
        actions$ = of(submitTypesAction({types}));
        subscription = effect.submit$.subscribe();
        expect(submitSpy).toHaveBeenCalledWith(types);
    });

    it("should call delete types method on submit action", () => {
        const deleteSpy = spyOn(effect, "deleteTypes").and.returnValue(EMPTY);
        const types = [{label: "label", delete: true}];
        actions$ = of(submitTypesAction({types}));
        subscription = effect.submit$.subscribe();
        expect(deleteSpy).toHaveBeenCalledWith(["label"], []);
    });

    it("should call add types method on submit action", () => {
        const addSpy = spyOn(effect, "addTypes").and.returnValue(EMPTY);
        const types = [{label: "label", add: true}];
        actions$ = of(submitTypesAction({types}));
        subscription = effect.submit$.subscribe();
        expect(addSpy).toHaveBeenCalledWith(["label"], []);
    });

    it("should add up errors on all adding and deleting of types", async () => {
        spyOn(effect.settingsApiService, "addNewType").and.throwError("addError");
        spyOn(effect.settingsApiService, "deleteType").and.throwError("deleteError");
        const types = [{label: "label", add: true}];
        actions$ = of(submitTypesAction({types}));
        subscription = effect.submit$.subscribe();
        const submitSpy = await effect.submit$.pipe(take(1)).toPromise();
        expect(submitSpy).toBeTruthy();
    });
});

