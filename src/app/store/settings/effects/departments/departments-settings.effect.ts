/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Action} from "@ngrx/store";
import {Observable} from "rxjs";
import {endWith, map, startWith, switchMap} from "rxjs/operators";
import {IAPIDepartmentTable, SettingsApiService} from "../../../../core";
import {catchErrorTo, catchHttpErrorTo, EHttpStatusCodes} from "../../../../util";
import {EErrorCode, setErrorAction} from "../../../root";
import {
    fetchDepartmentsSettingsAction,
    setDepartmentsSettingsAction,
    setSettingsLoadingStateAction,
    submitDepartmentsSettingsAction
} from "../../actions";

@Injectable({providedIn: "root"})
export class DepartmentsSettingsEffect {

    public fetch$ = createEffect(() => this.actions.pipe(
        ofType(fetchDepartmentsSettingsAction),
        switchMap(() => this.fetch())
    ));

    public submit$ = createEffect(() => this.actions.pipe(
        ofType(submitDepartmentsSettingsAction),
        switchMap((action) => this.submit(action.data))
    ));

    public constructor(public actions: Actions, public settingsApiService: SettingsApiService) {

    }

    public fetch(): Observable<Action> {
        return this.settingsApiService.getDepartmentTable().pipe(
            map((data) => setDepartmentsSettingsAction({data})),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setSettingsLoadingStateAction({state: {fetchingDepartments: true}})),
            endWith(setSettingsLoadingStateAction({state: {fetchingDepartments: false}}))
        );
    }

    public submit(data: IAPIDepartmentTable): Observable<Action> {
        return this.settingsApiService.putDepartmentTable(data).pipe(
            switchMap(() => this.fetch()),
            catchHttpErrorTo(setErrorAction({error: EErrorCode.INVALID_FILE_FORMAT}), EHttpStatusCodes.BAD_REQUEST),
            catchErrorTo(setErrorAction({error: EErrorCode.UNEXPECTED})),
            startWith(setSettingsLoadingStateAction({state: {submittingDepartments: true}})),
            endWith(setSettingsLoadingStateAction({state: {submittingDepartments: false}}))
        );
    }

}
