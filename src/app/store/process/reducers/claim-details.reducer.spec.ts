/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIClaimDetails} from "../../../core/api/process/IAPIClaimDetails";
import {setClaimDetailsAction} from "../actions";
import {claimDetailsReducer} from "./claim-details.reducer";

describe("claimDetailsReducer", () => {

    it("should set the claim details as state", () => {
        const action = setClaimDetailsAction({
            claimDetails: {
                statementId: 19,
                taskId: "taskId"
            } as unknown as IAPIClaimDetails
        });
        const claimDetailsStoreState = {
            taskId: {
                statementId: 19,
                taskId: "taskId"
            } as unknown as IAPIClaimDetails
        };
        expect(claimDetailsReducer({}, action)).toEqual(claimDetailsStoreState);
    });
});
