/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {Store} from "@ngrx/store";
import {provideMockStore} from "@ngrx/store/testing";
import {I18nModule, IAPIAttachmentModel} from "../../../../core";
import {IAttachmentControlValue, queryParamsIdSelector, startAttachmentDownloadAction} from "../../../../store";
import {StatementDetailsModule} from "../../statement-details.module";
import {StatementDetailsAttachmentsComponent} from "./statement-details-attachments.component";

describe("StatementDetailsAttachmentsComponent", () => {
    let component: StatementDetailsAttachmentsComponent;
    let fixture: ComponentFixture<StatementDetailsAttachmentsComponent>;
    let store: Store;

    const attachments: IAttachmentControlValue[] = [
        {name: "attachment1", tagIds: ["tag1", "email"]},
        {name: "attachment2", tagIds: ["email", "tag3"]},
        {name: "attachment3", tagIds: ["tag1", "tag2"]},
        {name: "attachment4", tagIds: ["tag1", "tag2", "email"]}
    ];

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                StatementDetailsModule,
                I18nModule
            ],
            providers: [
                provideMockStore({
                    initialState: {},
                    selectors: [
                        {
                            selector: queryParamsIdSelector,
                            value: 19
                        }
                    ]
                })
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(StatementDetailsAttachmentsComponent);
        component = fixture.componentInstance;
        store = fixture.componentRef.injector.get(Store);
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should return only the attachments containing at least one of the selected tags", () => {
        component.availableTags = [
            {label: "tag1", id: "tag1", isSelected: true},
            {label: "tag2", id: "tag2", isSelected: true},
            {label: "tag3", id: "tag3", isSelected: false}
        ];

        const result = component.filterBySelectedTags(attachments);
        expect(result).toEqual([
            {name: "attachment1", tagIds: ["tag1", "email"]},
            {name: "attachment3", tagIds: ["tag1", "tag2"]},
            {name: "attachment4", tagIds: ["tag1", "tag2", "email"]}
        ]);
    });

    it("should select all available tags", () => {
        component.availableTags = [
            {label: "tag1", id: "tag1", isSelected: true},
            {label: "tag2", id: "tag2", isSelected: false},
            {label: "tag3", id: "tag3", isSelected: false}
        ];
        component.deselectAllTags();
        expect(component.availableTags).toEqual([
            {label: "tag1", id: "tag1", isSelected: false},
            {label: "tag2", id: "tag2", isSelected: false},
            {label: "tag3", id: "tag3", isSelected: false}
        ]);
    });

    it("should toggle the tag state or set to given value", () => {
        const tag = {label: "tag1", id: "tag1", isSelected: true};
        component.toggleTag(tag);
        expect(tag.isSelected).toBeFalse();
        component.toggleTag(tag);
        expect(tag.isSelected).toBeTrue();
        component.toggleTag(tag, true);
        expect(tag.isSelected).toBeTrue();
    });

    it("should dispatch startAttachmentDownloadAction with correct attachmentId", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        await component.downloadAttachment(15);
        expect(dispatchSpy).toHaveBeenCalledWith(startAttachmentDownloadAction({statementId: 19, attachmentId: 15}));
    });

    it("should only return the email attachments", () => {
        const emailAttachments = component.filterForEmailAttachments(attachments as IAPIAttachmentModel[]);
        expect(emailAttachments).toEqual([
            {name: "attachment1", tagIds: ["tag1", "email"]},
            {name: "attachment2", tagIds: ["email", "tag3"]},
            {name: "attachment4", tagIds: ["tag1", "tag2", "email"]}
        ] as IAPIAttachmentModel[]);
    });
});
