/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {AfterViewInit, Component, ElementRef, Input, ViewChild} from "@angular/core";
import {timer} from "rxjs";
import {momentFormatDisplayFullDateAndTime} from "../../../../../util/moment";
import {IProcessHistoryData} from "./IProcessHistoryData";

@Component({
    selector: "app-process-history",
    templateUrl: "./process-history.component.html",
    styleUrls: ["./process-history.component.scss"]
})
export class ProcessHistoryComponent implements AfterViewInit {

    @ViewChild("table", {read: ElementRef}) table: ElementRef;

    @Input()
    public appProcessHistoryData: Array<IProcessHistoryData>;

    @Input()
    public timeDisplayFormat: string = momentFormatDisplayFullDateAndTime;

    public columnsToDisplay = ["icon", "action", "done", "user"];

    public async ngAfterViewInit() {
        await timer(400).toPromise();
        this.table.nativeElement.scrollTop = this.table.nativeElement.scrollHeight;
    }
}
