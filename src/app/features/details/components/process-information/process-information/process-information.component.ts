/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {IProcessHistoryData} from "../process-history";

/**
 * This component displays the process diagram (bpmn) and marks the current state the statement is in.
 * Also shows the process history as a table.
 */

@Component({
    selector: "app-process-information",
    templateUrl: "./process-information.component.html",
    styleUrls: ["./process-information.component.scss"]
})
export class ProcessInformationComponent {

    @Input()
    public appStatementId: number;

    @Input()
    public appProcessName: string;

    @Input()
    public appProcessVersion: number;

    @Input()
    public appProcessHistoryData: Array<IProcessHistoryData>;

    @Input()
    public appCurrentActivities: Array<string>;

    @Input()
    public appWorkflowXml: string;
}
