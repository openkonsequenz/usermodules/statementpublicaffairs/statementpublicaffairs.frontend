/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from "@angular/core";
import {select, Store} from "@ngrx/store";
import {Observable, Subject} from "rxjs";
import {take, takeUntil} from "rxjs/operators";
import {IAPISectorsModel, IAPIStatementModel} from "../../../../core";
import {IAPIContactPersonDetails} from "../../../../core/api/contacts/IAPIContactPersonDetails";
import {getStatementSectorsSelector, statementTypeOptionsSelector} from "../../../../store";
import {momentFormatDisplayNumeric} from "../../../../util";


/**
 * This component displays the general information saved to a statement.
 */

@Component({
    selector: "app-statement-details-information",
    templateUrl: "./statement-details-information.component.html",
    styleUrls: ["./statement-details-information.component.scss"]
})
export class StatementDetailsInformationComponent implements OnInit, OnDestroy, OnChanges {

    @Input()
    public appCollapsed = false;

    @Input()
    public appStatementInfo: IAPIStatementModel;

    @Input()
    public appContactInfo: IAPIContactPersonDetails;

    public statementTypeOptions$ = this.store.pipe(select(statementTypeOptionsSelector));

    public statementType: string;

    public sectors$: Observable<IAPISectorsModel> = this.store.pipe(select(getStatementSectorsSelector));

    public timeFormat = momentFormatDisplayNumeric;

    private destroy$ = new Subject<void>();

    public constructor(public store: Store) {

    }

    public ngOnInit() {
        this.statementTypeOptions$.pipe(
            takeUntil(this.destroy$)
        ).subscribe((typeOptions) => {
            this.statementType = typeOptions.find((_) => _?.value === this.appStatementInfo?.typeId)?.label;
        });
    }

    public ngOnDestroy() {
        this.destroy$.next();
        this.destroy$.complete();
    }

    public async ngOnChanges(changes: SimpleChanges) {
        if (changes.appStatementInfo) {
            const typeOptions = await this.statementTypeOptions$.pipe(take(1)).toPromise();
            this.statementType = typeOptions?.find((_) => _?.value === this.appStatementInfo?.typeId)?.label;
        }
    }

}
