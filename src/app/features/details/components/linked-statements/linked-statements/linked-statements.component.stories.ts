/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {RouterTestingModule} from "@angular/router/testing";
import {number, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../../core/i18n";
import {createSelectOptionsMock, createStatementModelMock} from "../../../../../test";
import {StatementDetailsModule} from "../../../statement-details.module";


storiesOf("Shared", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            RouterTestingModule,
            I18nModule,
            StatementDetailsModule
        ]
    }))
    .add("StatementDetailsLinkedStatementsComponent", () => ({
        template: `
            <div style="padding: 1em;">
                <app-linked-statements
                    [appParents]="entries.slice(0, numberOfRowsToDisplay)"
                    [appChildren]="entries.slice(0, numberOfRowsToDisplay)"
                    [appStatementTypeOptions]="appStatementTypeOptions">
                </app-linked-statements>
            </div>
        `,
        props: {
            entries: Array(20).fill(0)
                .map((_, id) => createStatementModelMock(id, id % 5)),
            appStatementTypeOptions: createSelectOptionsMock(5, "StatementType"),
            numberOfRowsToDisplay: number("number of rows to display", 4, {min: 0, max: 11}),
        }
    }));
