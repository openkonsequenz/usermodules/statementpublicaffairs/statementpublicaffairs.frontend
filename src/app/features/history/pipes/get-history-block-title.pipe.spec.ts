/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ITextblockHistoryVersionContentModel} from "../interfaces/ITextblockHistoryVersionContentModel";
import {GetHistoryBlockTitlePipe} from "./get-history-block-title.pipe";

describe("GetHistoryBlockTitle", () => {

    const pipe = new GetHistoryBlockTitlePipe();

    it("should return the correct title as string", () => {
        const model: ITextblockHistoryVersionContentModel = {
            type: "text",
            text: "text"
        } as ITextblockHistoryVersionContentModel;
        let result = pipe.transform(model);
        expect(result).toEqual("textBlocks.standardBlocks.freeText");

        result = pipe.transform({...model, type: "pagebreak"});
        expect(result).toEqual("textBlocks.standardBlocks.pagebreak");

        result = pipe.transform({...model, type: "newline"});
        expect(result).toEqual("textBlocks.standardBlocks.newLine");

        result = pipe.transform({...model, type: "block"});
        expect(result).toEqual(undefined);

        result = pipe.transform({...model, type: "block", id: "textBlockId"});
        expect(result).toEqual("textBlockId");

        result = pipe.transform(undefined);
        expect(result).toEqual(undefined);
    });
});


