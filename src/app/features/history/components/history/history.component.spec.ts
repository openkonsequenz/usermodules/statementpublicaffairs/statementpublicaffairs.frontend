/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {MockStore, provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core";
import {IAPITextblockHistoryModel} from "../../../../core/api/statements/IAPITextblockHistoryModel";
import {IStoreTextblockHistoryVersionModel} from "../../../../core/api/statements/IAPITextblockHistoryVersionModel";
import {statementTextblockHistorySelector} from "../../../../store/statements/selectors/textblock-history/textblock-history.selectors";
import {HistoryModule} from "../../history.module";
import {HistoryComponent} from "./history.component";

describe("HistoryComponent", () => {
    let component: HistoryComponent;
    let fixture: ComponentFixture<HistoryComponent>;
    let mockStore: MockStore;

    const history: IAPITextblockHistoryModel = {
        statementId: 11,
        versions: {
            3: {
                version: "3",
                content: [{
                    type: "block",
                    id: "id3",
                    diffType: "NEW",
                    prevVersion: null,
                    prevText: null,
                    text: "text3",
                    moved: "NOT"
                }]
            } as unknown as IStoreTextblockHistoryVersionModel,
            4: {
                version: "4",
                content: [{
                    type: "block",
                    id: "id4",
                    diffType: "CHANGED",
                    prevVersion: "3",
                    prevText: "text3",
                    text: "text4",
                    moved: "NOT"
                }]
            } as unknown as IStoreTextblockHistoryVersionModel
        }
    } as unknown as IAPITextblockHistoryModel;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            declarations: [HistoryComponent],
            imports: [I18nModule, RouterTestingModule, HistoryModule],
            providers: [provideMockStore({initialState: {statements: {}}})]
        }).compileComponents();
        mockStore = TestBed.inject(MockStore);
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HistoryComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should set the selected version from the dropdown", async () => {
        const history1: IAPITextblockHistoryModel = {
            statementId: 11,
            versions: {
                4: {
                    version: "4",
                    content: []
                } as unknown as IStoreTextblockHistoryVersionModel
            }
        } as unknown as IAPITextblockHistoryModel;
        mockStore.overrideSelector(statementTextblockHistorySelector, history);
        expect(component.selectedVersion).toBeFalsy();
        await component.selectVersion("4");
        fixture.detectChanges();
        expect(component.selectedVersion).toBeTruthy();
    });

    it("should set the latest version as soon as a list of versions is available", async () => {
        const history1: IAPITextblockHistoryModel = {
            statementId: 11,
            versionOrder: [
                "4",
                "5"
            ],
            versions: {
                4: {
                    version: "4"
                } as unknown as IStoreTextblockHistoryVersionModel,
                5: {
                    version: "5"
                } as unknown as IStoreTextblockHistoryVersionModel
            }
        } as unknown as IAPITextblockHistoryModel;
        mockStore.overrideSelector(statementTextblockHistorySelector, history1);
        await component.ngOnInit();
        fixture.detectChanges();
        await fixture.whenStable();
        expect(component.lastVersion).toBeTruthy();
        expect(component.lastVersion).toEqual(history1.versions["5"]);
    });

    it("should add information about the user to the content of a history version", async () => {
        mockStore.overrideSelector(statementTextblockHistorySelector, history);
        await component.selectVersion("4");
        fixture.detectChanges();
        expect(component.selectedVersion).toBeTruthy();
        expect(component.selectedVersion.content[0].version).toEqual(history.versions["4"].version);
        expect(component.selectedVersion.content[0].timestamp).toEqual(history.versions["4"].timestamp);
        expect(component.selectedVersion.content[0].prevVersion.version).toEqual(history.versions["3"].version);
        expect(component.selectedVersion.content[0].prevVersion.timestamp).toEqual(history.versions["3"].timestamp);
    });

    it("should toggle between normal and details mode", () => {
        expect(component.selectedMode.value).toEqual("normal");
        component.changeMode();
        expect(component.selectedMode.value).toEqual("details");
        component.changeMode();
        expect(component.selectedMode.value).toEqual("normal");
    });

    it("should select the version a certain number off the currently selected one", async () => {
        mockStore.overrideSelector(statementTextblockHistorySelector, history);
        await component.selectVersion("4");
        fixture.detectChanges();
        const firstSelectedVersion = component.selectedVersion;
        expect(component.selectedVersion).toBeTruthy();
        expect(component.selectedVersion.version).toEqual("4");
        await component.selectVersionFromIndex(-1);
        expect(component.selectedVersion).toBeTruthy();
        expect(component.selectedVersion.version).toEqual("3");
        await component.selectVersionFromIndex(-1);
        expect(component.selectedVersion).toBeTruthy();
        expect(component.selectedVersion.version).toEqual("3");
        await component.selectVersionFromIndex(1);
        expect(component.selectedVersion).toBeTruthy();
        expect(component.selectedVersion.version).toEqual("4");
        await component.selectVersionFromIndex(1);
        expect(component.selectedVersion).toBeTruthy();
        expect(component.selectedVersion.version).toEqual("4");
    });

});
