/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, forwardRef, Input} from "@angular/core";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {momentFormatDisplayFullDateAndTime} from "../../../../util";
import {ITextblockHistoryVersionContentModel} from "../../interfaces/ITextblockHistoryVersionContentModel";

@Component({
    selector: "app-display-textblock-details",
    templateUrl: "./display-textblock-details.component.html",
    styleUrls: ["./display-textblock-details.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DisplayTextblockDetailsComponent),
            multi: true
        }
    ]
})
export class DisplayTextblockDetailsComponent {

    @Input()
    public appTextBlockModel: ITextblockHistoryVersionContentModel;

    public timeFormat = momentFormatDisplayFullDateAndTime;
}
