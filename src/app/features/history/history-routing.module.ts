/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {HistoryComponent} from "./components/history";
import {HistoryModule} from "./history.module";

const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        component: HistoryComponent
    }
];

@NgModule({
    imports: [
        HistoryModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class HistoryRoutingModule {

}
