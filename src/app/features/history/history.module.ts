/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatIconModule} from "@angular/material/icon";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {DateControlModule} from "../../shared/controls/date-control";
import {SelectModule} from "../../shared/controls/select";
import {ActionButtonModule} from "../../shared/layout/action-button";
import {CollapsibleModule} from "../../shared/layout/collapsible";
import {TextBlockModule} from "../../shared/text-block";
import {StatementDetailsModule} from "../details";
import {StatementInformationFormModule} from "../forms/statement-information";
import {DisplayTextblockComponent} from "./components/display-textblock";
import {DisplayTextblockDetailsComponent} from "./components/display-textblock-details";
import {DisplayUserInfoComponent} from "./components/display-user-info";
import {DisplayVersionComponent} from "./components/display-version";
import {DisplayVersionDetailsComponent} from "./components/display-version-details";
import {HistoryComponent} from "./components/history";
import {VersionInfoComponent} from "./components/version-info";
import {FilterOutDeletedBlocksPipe} from "./pipes/filter-out-deleted-blocks.pipe";
import {GetHistoryBlockTitlePipe} from "./pipes/get-history-block-title.pipe";
import {IsDetailsModeDetectedPipe} from "./pipes/is-details-mode-selected.pipe";
import {IsNextIndexAvailablePipe} from "./pipes/is-next-index-available.pipe";

@NgModule({
    imports: [
        CommonModule,
        MatIconModule,
        TranslateModule,
        RouterModule,
        MatIconModule,

        CollapsibleModule,
        ActionButtonModule,
        StatementInformationFormModule,
        SelectModule,
        StatementDetailsModule,
        TextBlockModule,
        DateControlModule
    ],
    declarations: [
        HistoryComponent,
        DisplayVersionComponent,
        DisplayTextblockComponent,
        VersionInfoComponent,
        DisplayVersionDetailsComponent,
        DisplayTextblockDetailsComponent,
        DisplayUserInfoComponent,
        GetHistoryBlockTitlePipe,
        FilterOutDeletedBlocksPipe,
        IsDetailsModeDetectedPipe,
        IsNextIndexAvailablePipe
    ],
    exports: [
        HistoryComponent,
        DisplayVersionComponent,
        DisplayVersionDetailsComponent,
        DisplayTextblockComponent,
        DisplayTextblockDetailsComponent
    ]
})
export class HistoryModule {

}
