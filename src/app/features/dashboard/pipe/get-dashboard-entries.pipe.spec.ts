/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {EAPIProcessTaskDefinitionKey, IAPIProcessTask} from "../../../core/api/process";
import {IStatementTableEntry} from "../../../shared/layout/statement-table/model";
import {IStatementEntityWithTasks} from "../../../store/statements/model";
import {createStatementModelMock} from "../../../test";
import {GetDashboardEntriesPipe} from "./get-dashboard-entries.pipe";

describe("GetDashboardEntriesPipe", () => {

    const pipe = new GetDashboardEntriesPipe();

    it("should transform a list of statements to table entries", () => {
        const entities: IStatementEntityWithTasks[] = Array(100).fill(0).map((_, id) => ({
            info: createStatementModelMock(id)
        }));

        entities.push(null);
        entities.push({});

        expect(pipe.transform(entities)).toEqual(entities
            .filter((_) => _?.info?.id != null)
            .map((_) => ({
                ..._.info,
                contributionStatus: "-",
                contributionStatusForMyDepartment: false,
                currentTaskName: undefined,
                departmentStandIn: undefined
            }))
        );

        entities[19] = {
            ...entities[19],
            editedByMe: true,
            mandatoryContributionsCount: 10,
            mandatoryDepartmentsCount: 19,
            completedForMyDepartment: true,
            optionalForMyDepartment: true,
            tasks: [{
                ...{} as IAPIProcessTask,
                taskDefinitionKey: EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA
            }]
        };

        const expectedResult: IStatementTableEntry[] = [{
            ...entities[19].info,
            contributionStatus: "10/19",
            contributionStatusForMyDepartment: true,
            currentTaskName: EAPIProcessTaskDefinitionKey.ADD_BASIC_INFO_DATA,
            departmentStandIn: undefined
        }];

        expect(pipe.transform(entities, true)).toEqual(expectedResult);

        entities[19].completedForMyDepartment = false;
        expectedResult[0].contributionStatusForMyDepartment = null;
        expect(pipe.transform(entities, true)).toEqual(expectedResult);

        entities[19].tasks[0].name = "TaskName";
        expectedResult[0].currentTaskName = "TaskName";
        expect(pipe.transform(entities, true)).toEqual(expectedResult);
    });

});
