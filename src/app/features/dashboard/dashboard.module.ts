/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatIconModule} from "@angular/material/icon";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {SideMenuModule} from "../../shared/layout/side-menu";
import {StatementTableModule} from "../../shared/layout/statement-table";
import {SharedPipesModule} from "../../shared/pipes";
import {ProgressSpinnerModule} from "../../shared/progress-spinner";
import {DashboardComponent, DashboardListComponent} from "./components";
import {GetDashboardEntriesPipe} from "./pipe";

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MatIconModule,
        SharedPipesModule,
        StatementTableModule,
        ProgressSpinnerModule,
        SideMenuModule,
        TranslateModule
    ],
    declarations: [
        DashboardComponent,
        DashboardListComponent,
        GetDashboardEntriesPipe
    ],
    exports: [
        DashboardComponent,
        DashboardListComponent,
        GetDashboardEntriesPipe
    ]
})
export class DashboardModule {

}
