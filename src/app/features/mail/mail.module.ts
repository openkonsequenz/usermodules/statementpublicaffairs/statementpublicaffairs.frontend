/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatIconModule} from "@angular/material/icon";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {DateControlModule} from "../../shared/controls/date-control";
import {ActionButtonModule} from "../../shared/layout/action-button";
import {CollapsibleModule} from "../../shared/layout/collapsible";
import {SideMenuModule} from "../../shared/layout/side-menu";
import {SharedPipesModule} from "../../shared/pipes";
import {ProgressSpinnerModule} from "../../shared/progress-spinner";
import {MailComponent, MailDetailsComponent, MailInboxComponent} from "./components";
import {DivideSenderAndMailPipe} from "./pipes/divide-sender-and-mail.pipe";
import {EmailTextToArrayPipe} from "./pipes/email-text.pipe";
import {ExtractMailAddressPipe} from "./pipes/extract-mail-address.pipe";

@NgModule({
    imports: [
        CommonModule,
        SideMenuModule,
        RouterModule,
        SharedPipesModule,
        ActionButtonModule,
        MatIconModule,
        ProgressSpinnerModule,
        DateControlModule,
        CollapsibleModule,
        TranslateModule
    ],
    declarations: [
        MailComponent,
        MailInboxComponent,
        MailDetailsComponent,
        EmailTextToArrayPipe,
        DivideSenderAndMailPipe,
        ExtractMailAddressPipe
    ],
    exports: [
        MailComponent,
        MailInboxComponent,
        MailDetailsComponent
    ]
})
export class MailModule {

}
