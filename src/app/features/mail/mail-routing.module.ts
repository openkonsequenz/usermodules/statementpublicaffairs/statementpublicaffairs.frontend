/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {OfficialInChargeRouteGuardService} from "../../store/root/services";
import {MailComponent} from "./components";
import {MailModule} from "./mail.module";

const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        component: MailComponent,
        canActivate: [OfficialInChargeRouteGuardService]
    }
];

@NgModule({
    imports: [
        MailModule,
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class MailRoutingModule {

}
