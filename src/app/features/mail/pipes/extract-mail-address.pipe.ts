/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {arrayJoin} from "../../../util/store";

/**
 * From given string with format: <name e@mail.com>
 * returns only the email: "e@mail.com"
 */

@Pipe({
    name: "appExtractMailAddress"
})
export class ExtractMailAddressPipe implements PipeTransform {

    public transform(text: string): string {
        const splitText = text?.split("<");
        return arrayJoin(splitText)[1] ? arrayJoin(splitText)[1].replace(">", "") : text;
    }
}
