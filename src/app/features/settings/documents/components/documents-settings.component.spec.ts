/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {Store} from "@ngrx/store";
import {provideMockStore} from "@ngrx/store/testing";
import {I18nModule} from "../../../../core";
import {submitTypesAction} from "../../../../store/statements/actions";
import {DocumentsSettingsModule} from "../documents.settings.module";
import {DocumentsSettingsComponent} from "./documents-settings.component";

describe("TagSettingsComponent", () => {
    let component: DocumentsSettingsComponent;
    let fixture: ComponentFixture<DocumentsSettingsComponent>;
    let store: Store;

    const tags = [
        {label: "label", add: true},
        {label: "label1"},
        {label: "label2", delete: true},
        {label: "label3", add: true}
    ];

    const types = [
        {label: "label", add: true},
        {label: "label1"},
        {label: "label2", delete: true},
        {label: "label3", add: true}
    ];

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                DocumentsSettingsModule,
                RouterTestingModule,
                I18nModule
            ],
            providers: [
                provideMockStore()
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DocumentsSettingsComponent);
        component = fixture.componentInstance;
        store = fixture.componentRef.injector.get(Store);
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should add string to taglist if set", () => {
        expect(component.tagList).toEqual([]);
        component.addTag(null);
        expect(component.tagList).toEqual([]);
        component.addTag("");
        expect(component.tagList).toEqual([]);
        component.addTag("test");
        expect(component.tagList.length).toEqual(1);
        expect(component.tagList).toEqual([{label: "test", add: true}]);
    });

    it("should delete the specified entry from taglist", () => {
        component.tagList = tags;
        component.deleteTag(14);
        expect(component.tagList).toEqual(tags);
        component.deleteTag(2);
        expect(component.tagList).toEqual([
            {label: "label", add: true},
            {label: "label1"},
            {label: "label3", add: true}
        ]);
    });

    it("should dispatch submitTypesAction with labels of types to be added", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        component.typeList = types;
        await component.save();
        expect(dispatchSpy).toHaveBeenCalledWith(submitTypesAction({
            types: [
                {label: "label", add: true, delete: undefined},
                {label: "label2", add: undefined, delete: true},
                {label: "label3", add: true, delete: undefined}
            ]
        }));
    });

    it("should not dispatch submitTagsAction if no tags or types are edited", async () => {
        const dispatchSpy = spyOn(store, "dispatch");
        component.tagList = [];
        component.typeList = [];
        await component.save();
        expect(dispatchSpy).not.toHaveBeenCalled();
    });

    it("should delete tag from list if not standard and in range", () => {
        component.tagList = [{label: "label"}];
        component.deleteTag(0);
        expect(component.tagList.length).toEqual(0);
        component.tagList = [{label: "label"}];
        component.deleteTag(1);
        expect(component.tagList.length).toEqual(1);
        expect(component.tagList[0].delete).toEqual(undefined);
        component.tagList = [{label: "label", isStandard: true}];
        component.deleteTag(0);
        expect(component.tagList.length).toEqual(1);
        expect(component.tagList[0].delete).toEqual(undefined);
        component.tagList = [{label: "label", initial: true}];
        component.deleteTag(0);
        expect(component.tagList.length).toEqual(1);
        expect(component.tagList[0].delete).toEqual(true);
    });

    it("should delete type from list if in range", () => {
        component.typeList = [{label: "label"}];
        component.deleteType(0);
        expect(component.typeList.length).toEqual(0);
        component.typeList = [{label: "label"}];
        component.deleteType(1);
        expect(component.typeList.length).toEqual(1);
        expect(component.typeList[0].delete).toEqual(undefined);
        component.typeList = [{label: "label", initial: true}];
        component.deleteType(0);
        expect(component.typeList.length).toEqual(1);
        expect(component.typeList[0].delete).toEqual(true);
    });

    it("should restore tag", () => {
        component.tagList = [{label: "label", delete: true}];
        expect(component.tagList[0].delete).toEqual(true);
        component.restoreTag(0);
        expect(component.tagList[0].delete).toEqual(false);
        component.restoreTag(1);
        expect(component.tagList[0].delete).toEqual(false);
    });

    it("should restore type", async () => {
        component.typeList = [{label: "label", delete: true}];
        expect(component.typeList[0].delete).toEqual(true);
        component.restoreType(0);
        expect(component.typeList[0].delete).toEqual(false);
        component.restoreType(1);
        expect(component.typeList[0].delete).toEqual(false);
    });

    it("should add a tag only if input was made", () => {
        component.tagList = [];
        component.addTag(null);
        expect(component.tagList.length).toEqual(0);
        component.addTag("label");
        expect(component.tagList.length).toEqual(1);
        expect(component.tagList[0].label).toEqual("label");
    });

    it("should add a type only if input was made", () => {
        component.typeList = [];
        component.addType(null);
        expect(component.typeList.length).toEqual(0);
        component.addType("label");
        expect(component.typeList.length).toEqual(1);
        expect(component.typeList[0].label).toEqual("label");
    });
});
