/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {IAPITextBlockModel} from "../../../../../core/api/text";
import {I18nModule} from "../../../../../core/i18n";
import {createTextBlockModelMock} from "../../../../../test";
import {TextBlockSettingsModule} from "../../text-block-settings.module";
import {TextBlockSettingsForm} from "../../TextBlockSettingsForm";
import {TextBlockSelectFormComponent} from "./text-block-select-form.component";

describe("TextBlockSelectFormComponent", () => {
    const keyName = "test";
    let component: TextBlockSelectFormComponent;
    let fixture: ComponentFixture<TextBlockSelectFormComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [TextBlockSettingsModule, I18nModule],
            providers: [{provide: TextBlockSettingsForm, useValue: new TextBlockSettingsForm()}]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(TextBlockSelectFormComponent);
        component = fixture.componentInstance;
        component.form.setForm({
            selects: {
                test: []
            },
            groups: [{
                groupName: "Group",
                textBlocks: [{...createTextBlockModelMock("19", createToken(keyName))}]
            }],
            negativeGroups: []
        });
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should automatically select first key", () => {
        expect(component.selectedKey).toBe(keyName);
    });

    it("should add/remove select entries", () => {
        component.appDefaultSelectEntry = "Test";
        component.setEditedKey(keyName);
        component.addSelectEntry(keyName);
        expect(component.form.getSelectEntries(keyName)).toEqual(["Test"]);
        component.removeSelectEntry(keyName, 0);
        expect(component.form.getSelectEntries(keyName)).toEqual([]);
    });

    it("should generate/remove select keys", () => {
        component.appDefaultSelectKey = keyName;
        component.createNewSelectKey();
        expect(component.form.getSelectKeys()).toEqual([keyName, keyName + "1"]);
        component.removeSelectKey(keyName + "1");
        component.removeSelectKey(keyName);
        expect(component.form.getSelectKeys()).toEqual([]);
        forEachTextBlock((textBlock) => {
            expect(textBlock.text).toEqual("");
        });
    });

    it("should change select keys", () => {
        const newKeyName = "newKey";
        component.changeSelectKey(keyName, newKeyName);
        expect(component.form.getSelectKeys()).toEqual([newKeyName]);
        forEachTextBlock((textBlock) => expect(textBlock.text).toEqual(createToken(newKeyName)));
    });

    it("should verify key names", () => {
        component.appDefaultSelectKey = keyName;
        component.createNewSelectKey();
        const newKeyName = component.form.getSelectKeys().reverse()[0];

        expect(component.editedKey).toBe(newKeyName);
        expect(component.selectedKey).toBe(newKeyName);
        expect(component.isEditedKeyValid).toBe(true);

        component.changeSelectKey(newKeyName, newKeyName);
        expect(component.editedKey).toBe(newKeyName);
        expect(component.selectedKey).toBe(newKeyName);
        expect(component.isEditedKeyValid).toBe(true);

        [keyName, "Another Invalid Keyname"].forEach((invalidKeyName) => {
            component.changeSelectKey(newKeyName, invalidKeyName);
            expect(component.selectedKey).toBe(newKeyName);
            expect(component.editedKey).toBe(invalidKeyName);
            expect(component.isEditedKeyValid).toBe(false);
        });
    });

    function forEachTextBlock(fn: (textBlock: IAPITextBlockModel) => any) {
        component.form.getValue().groups.forEach((group) => group.textBlocks.forEach(fn));
    }

    function createToken(key: string) {
        return `<s:${key}>`;
    }
});
