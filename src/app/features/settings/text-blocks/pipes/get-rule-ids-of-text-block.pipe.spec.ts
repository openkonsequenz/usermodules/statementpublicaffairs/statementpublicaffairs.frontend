/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPITextBlockModel} from "../../../../core/api/text";
import {GetRuleIdsOfTextBlockPipe} from "./get-rule-ids-of-text-block.pipe";

describe("GetRuleIdsOfTextBlockPipe", () => {

    let pipe: GetRuleIdsOfTextBlockPipe;

    beforeEach(() => {
        pipe = new GetRuleIdsOfTextBlockPipe();
    });

    it("should extract list of IDs for a given rule", () => {
        const textBlock: IAPITextBlockModel = {
            id: "1.9",
            text: "",
            requires: [
                {
                    type: "and",
                    ids: ["1.119"]
                }
            ],
            excludes: ["1.7", "1.19"]
        };

        expect(pipe.transform(null, "excludes")).toEqual([]);
        expect(pipe.transform(textBlock, null)).toEqual([]);
        expect(pipe.transform(textBlock, "requires")).toEqual([]);
        expect(pipe.transform(textBlock, "requires", "xor")).toEqual([]);
        expect(pipe.transform(textBlock, "excludes")).toEqual(textBlock.excludes);
        expect(pipe.transform(textBlock, "requires", "and")).toEqual(textBlock.requires[0].ids);
    });


});
