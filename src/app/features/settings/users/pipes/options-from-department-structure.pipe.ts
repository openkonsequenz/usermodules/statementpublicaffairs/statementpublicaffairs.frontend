/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {arrayJoin} from "../../../../util";

@Pipe({
    name: "appOptionsFromDepartmentStructure"
})
export class OptionsFromDepartmentStructurePipe implements PipeTransform {

    public constructor(private translationService: TranslateService) {
    }

    public async transform(departments: { key: string; value: string[] }[]): Promise<{ label: string; value: string }[]> {
        const translation = await this.translationService.get("settings.users.noDepartment").toPromise();
        return arrayJoin(
            [{label: translation, value: null}],
            departmentsToGroupOptions(departments)
        );
    }

}

export function departmentsToGroupOptions(departments: { key: string; value: string[] }[]) {
    return arrayJoin(departments).map((_) => ({label: _.key, value: _.key}));
}
