/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {EAPIUserRoles, IAPIUserInfoExtended} from "../../../../core";
import {IssUserOfficialInChargePipe} from "./is-user-official-in-charge.pipe";

describe("IsUserOfficialInChargePipe", () => {

    const pipe = new IssUserOfficialInChargePipe();
    const divisionMember: IAPIUserInfoExtended = {
        roles: [
            EAPIUserRoles.DIVISION_MEMBER,
            EAPIUserRoles.SPA_CUSTOMER
        ]
    } as IAPIUserInfoExtended;
    const officialInCharge: IAPIUserInfoExtended = {
        roles: [
            EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE
        ]
    } as IAPIUserInfoExtended;

    describe("transform", () => {

        it("should return if the given user has role division member", () => {
            let result = pipe.transform(divisionMember);
            expect(result).toBeFalse();
            result = pipe.transform(officialInCharge);
            expect(result).toBeTrue();
        });
    });
});

