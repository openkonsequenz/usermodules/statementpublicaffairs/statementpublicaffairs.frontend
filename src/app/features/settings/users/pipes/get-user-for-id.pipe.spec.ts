/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIUserInfoExtended} from "../../../../core";
import {GetUserForIdPipe} from "./get-user-for-id.pipe";

describe("GetUserForIdPipe", () => {

    const pipe = new GetUserForIdPipe();
    const user = {id: 2} as IAPIUserInfoExtended;
    const users: IAPIUserInfoExtended[] = [
        {id: 1},
        user,
        {id: 3},
        {id: 4}
    ] as IAPIUserInfoExtended[];

    describe("transform", () => {

        it("should return the user with the given id", () => {
            const result = pipe.transform(users, 2);
            expect(result).toBeTruthy();
            expect(result).toEqual(user);
        });
    });
});

