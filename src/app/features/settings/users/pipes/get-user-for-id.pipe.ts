/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IAPIUserInfoExtended} from "../../../../core";

@Pipe({
    name: "appGetUserForId"
})
export class GetUserForIdPipe implements PipeTransform {

    public transform(users: IAPIUserInfoExtended[], id: number): IAPIUserInfoExtended {
        return users.find((_) => _.id === id);
    }

}

