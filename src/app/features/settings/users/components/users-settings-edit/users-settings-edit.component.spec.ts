/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {SimpleChange} from "@angular/core";
import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {I18nModule, IAPIUserInfoExtended, IAPIUserSettings} from "../../../../../core";
import {EErrorCode} from "../../../../../store";
import {UsersSettingsModule} from "../../users-settings.module";
import {IUserSettingsEditFormValue, UsersSettingsEditComponent} from "./users-settings-edit.component";

describe("UsersSettingsEditComponent", () => {
    let component: UsersSettingsEditComponent;
    let fixture: ComponentFixture<UsersSettingsEditComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                UsersSettingsModule,
                I18nModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UsersSettingsEditComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should disable form", () => {
        expect(component.formGroup.disabled).toBe(false);
        component.appDisabled = true;
        component.ngOnChanges({
            appDisabled: new SimpleChange(false, component.appDisabled, false)
        });
        expect(component.formGroup.disabled).toBe(true);
    });

    it("should patch form", () => {
        const email = "A";
        const fax = "fax";
        const phone = "phone";
        const initials = "initials";
        const departmentName = "B";
        const departmentGroupName = "C";
        const value: IUserSettingsEditFormValue = {
            email,
            fax,
            phone,
            initials,
            departments: [
                {name: departmentName, group: departmentGroupName, standIn: undefined},
                {name: "D", group: "E", standIn: true}
            ]
        };
        component.appSelectedUser = {
            ...{} as IAPIUserInfoExtended,
            settings: {
                email,
                fax,
                phone,
                initials,
                departments: [
                    {name: departmentName, group: departmentGroupName, standIn: undefined},
                    {name: "D", group: "E", standIn: true}
                ]
            }
        };
        component.ngOnChanges({
            appSelectedUser: new SimpleChange(null, component.appSelectedUser, false)
        });
        expect(component.formGroup.value).toEqual(value);
    });

    it("should set a default entry for departments", () => {
        component.appSelectedUser = null;
        component.ngOnChanges({
            appSelectedUser: new SimpleChange(null, null, false)
        });
        expect(component.formGroup.value.departments).toEqual([
            {name: null, group: null, standIn: false}
        ]);
    });

    it("should submit form", () => {
        const id = 19;
        const email = "abc@ef.gh";
        const fax = "fax";
        const phone = "phone";
        const initials = "initials";
        const departmentName = "B";
        const departmentGroupName = "C";
        const formValue: IUserSettingsEditFormValue = {email} as IUserSettingsEditFormValue;
        const userSettings: IAPIUserSettings = {email, fax, phone, initials};
        spyOn(component.appError, "emit");
        spyOn(component.appSubmit, "emit");

        component.patchValue({...formValue, email: "abc", phone: "phone", fax: "fax", initials: "initials"});
        component.addDepartmentEntry();
        component.patchValueForDepartment(0, {name: departmentName, group: departmentGroupName});
        component.submit(id);
        expect(component.appError.emit).toHaveBeenCalledWith(EErrorCode.BAD_USER_DATA);

        component.patchValueForDepartment(0, {name: null, group: departmentGroupName});
        component.submit(id);
        expect(component.appError.emit).toHaveBeenCalledWith(EErrorCode.MISSING_FORM_DATA);

        component.patchValue({...formValue});
        component.patchValueForDepartment(0, {name: departmentName, group: departmentGroupName});
        component.formGroup.updateValueAndValidity();
        component.submit(id);
        expect(component.appSubmit.emit).toHaveBeenCalledWith({
            id,
            value: {...userSettings, departments: [{name: departmentName, group: departmentGroupName, standIn: false}]}
        });
    });

    it("should delete department if index is in range", () => {
        spyOn(component.departments, "removeAt");
        component.addDepartmentEntry();
        component.addDepartmentEntry();
        component.addDepartmentEntry();

        component.deleteDepartmentEntry(-1);
        expect(component.departments.removeAt).not.toHaveBeenCalled();
        component.deleteDepartmentEntry(4);
        expect(component.departments.removeAt).not.toHaveBeenCalled();
        component.deleteDepartmentEntry(2);
        expect(component.departments.removeAt).toHaveBeenCalledWith(2);
    });

    it("should set error if same departments are added multiple times", () => {
        spyOn(component.appError, "emit");
        component.addDepartmentEntry();
        component.addDepartmentEntry();
        component.patchValueForDepartment(0, {name: "name", group: "groupName"});
        component.patchValueForDepartment(1, {name: "name", group: "groupName"});
        component.submit(1);
        expect(component.appError.emit).toHaveBeenCalledWith(EErrorCode.DEPARTMENT_MULTIPLE_USE);

        component.patchValueForDepartment(1, {name: "anotherName", group: "anotherGroupName"});
        component.patchValue({...{}, email: "abc", phone: "phone", fax: "fax", initials: "initials"});
        component.submit(1);
        expect(component.appError.emit).toHaveBeenCalledWith(EErrorCode.BAD_USER_DATA);
    });
});
