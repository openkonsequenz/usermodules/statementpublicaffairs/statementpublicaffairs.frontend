/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {I18nModule} from "../../../../../core/i18n";
import {UsersSettingsModule} from "../../users-settings.module";
import {UsersSettingsSearchComponent} from "./users-settings-search.component";

describe("UsersSettingsSearchComponent", () => {
    let component: UsersSettingsSearchComponent;
    let fixture: ComponentFixture<UsersSettingsSearchComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                UsersSettingsModule,
                I18nModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(UsersSettingsSearchComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should set filter parameter", () => {
        const departmentName = "test";
        const departmentGroupName = "testGroup";
        component.setFilterParameter("departmentName", departmentName);
        expect(component.appValue).toEqual({departmentName});

        component.setFilterParameter("departmentGroupName", departmentGroupName);
        expect(component.appValue).toEqual({departmentName, departmentGroupName});

        component.setFilterParameter("departmentGroupName", departmentGroupName, true);
        expect(component.appValue).toEqual({departmentGroupName});
    });

    it("should toggle filter parameter", () => {
        const departmentName = "test";
        const departmentGroupName = "testGroup";
        component.toggleFilterParameter("departmentName", departmentName);
        expect(component.appValue).toEqual({departmentName});

        component.toggleFilterParameter("departmentGroupName", departmentGroupName);
        expect(component.appValue).toEqual({departmentName, departmentGroupName});

        component.toggleFilterParameter("departmentGroupName", departmentGroupName);
        expect(component.appValue).toEqual({departmentName, departmentGroupName: undefined});
    });

});
