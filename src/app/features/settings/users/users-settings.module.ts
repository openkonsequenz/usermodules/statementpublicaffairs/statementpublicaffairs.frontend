/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CdkTableModule} from "@angular/cdk/table";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {TranslateModule} from "@ngx-translate/core";
import {CommonControlsModule} from "../../../shared/controls/common";
import {SelectModule} from "../../../shared/controls/select";
import {ActionButtonModule} from "../../../shared/layout/action-button";
import {CollapsibleModule} from "../../../shared/layout/collapsible";
import {SearchbarModule} from "../../../shared/layout/searchbar";
import {SideMenuModule} from "../../../shared/layout/side-menu";
import {SharedPipesModule} from "../../../shared/pipes";
import {SharedSettingsModule} from "../shared";
import {UsersSettingsComponent, UsersSettingsEditComponent, UsersSettingsSearchComponent, UsersSettingsTableComponent} from "./components";
import {
    FilterUserListPipe,
    GetUserForIdPipe,
    IsUserDivisionMemberPipe,
    OptionsForDepartmentGroupPipe,
    OptionsFromDepartmentStructurePipe,
    RolesToDisplayTextPipe
} from "./pipes";
import {IssUserOfficialInChargePipe} from "./pipes/is-user-official-in-charge.pipe";

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        MatIconModule,
        ReactiveFormsModule,
        CdkTableModule,

        SharedSettingsModule,
        ActionButtonModule,
        CollapsibleModule,
        SearchbarModule,
        SelectModule,
        SideMenuModule,
        CommonControlsModule,
        SharedPipesModule
    ],
    declarations: [
        UsersSettingsSearchComponent,
        UsersSettingsEditComponent,
        UsersSettingsTableComponent,
        UsersSettingsComponent,

        FilterUserListPipe,
        GetUserForIdPipe,
        IsUserDivisionMemberPipe,
        OptionsForDepartmentGroupPipe,
        OptionsFromDepartmentStructurePipe,
        RolesToDisplayTextPipe,
        IssUserOfficialInChargePipe
    ],
    exports: [
        UsersSettingsSearchComponent,
        UsersSettingsEditComponent,
        UsersSettingsTableComponent,
        UsersSettingsComponent,

        FilterUserListPipe,
        GetUserForIdPipe,
        IsUserDivisionMemberPipe,
        OptionsForDepartmentGroupPipe,
        OptionsFromDepartmentStructurePipe,
        RolesToDisplayTextPipe
    ]
})
export class UsersSettingsModule {

}
