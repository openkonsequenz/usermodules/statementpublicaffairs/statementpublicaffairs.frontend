/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIDepartmentTable} from "../../../../core/api/settings";
import {SectorsFromDepartmentPipe} from "./sectors-from-department-object.pipe";

describe("SectorsFromDepartmentPipe", () => {

    const pipe = new SectorsFromDepartmentPipe();

    const departmentTable: IAPIDepartmentTable = {
        "Ort#Ortsteil": {
            provides: [
                "Gas",
                "Strom",
                "Wasser"
            ],
            departments: {
                Allgemein: [
                    "Medianet",
                    "Planung"
                ],
                Weiteres: [
                    "Planung",
                    "Strom Beratung"
                ]
            }
        },
        "Zweiter#Ort": {
            provides: [
                "Beleuchtung"
            ],
            departments: {}
        }
    };

    describe("transform", () => {

        it("should return sectors from IAPIDepartment table object as a single string sorted alphabetically", () => {
            let result = pipe.transform(departmentTable);
            expect(result).toEqual(["Beleuchtung", "Gas", "Strom", "Wasser"]);

            result = pipe.transform(null);
            expect(result).toEqual([]);

            result = pipe.transform(undefined);
            expect(result).toEqual([]);

            result = pipe.transform({});
            expect(result).toEqual([]);
        });
    });
});

