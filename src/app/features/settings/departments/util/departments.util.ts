/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIDepartmentTable} from "../../../../core";
import {arrayJoin, parseCsv, reduceRowToCsv, reduceToCsv} from "../../../../util";

export function parseDepartmentTableFromCsv(text: string): IAPIDepartmentTable {
    return parseCsv(text, ";").slice(1)
        .map((row) => row.map((entry) => entry.trim()))
        .filter((row) => row.length > 1)
        .reduce<IAPIDepartmentTable>((result, value) => {
        if (value.length < 4) {
            throw new Error("Invalid format");
        }
        if (value.slice(0, 2).some((_) => _.length === 0)) {
            return result;
        }
        const key: string = value[0] + "#" + value[1];
        const locationDesignation: IAPIDepartmentTable[""]["locationDesignation"] = value[2];
        const provides: IAPIDepartmentTable[""]["provides"] = value[3].split(",").filter((sector) => sector.length > 0);
        const departments: IAPIDepartmentTable[""]["departments"] = value.slice(4)
            .reduce((dept, groupName, index, array) => {
                const name: string = array[index + 1];
                return index % 2 !== 0 || [groupName, name].some((_) => _.length === 0) ? dept : {
                    ...dept,
                    [groupName]: arrayJoin(dept[groupName], [name])
                };
            }, {});

        return {
            ...result,
            [key]: {locationDesignation, provides, departments}
        };
    }, {});
}


export function reduceDepartmentTableToCsv(table: IAPIDepartmentTable) {
    const headers = ["City", "District", "Location designation", "Sectors", "Departments"];
    const data: string[][] = Object.entries(table).map(([cityDistrict, entry]) => arrayJoin(
        [cityDistrict.replace("#", ";")],
        [entry.locationDesignation],
        [reduceRowToCsv(entry.provides, ",")],
        ...Object.entries(entry.departments)
            .map<string[]>(([groupName, names]) => arrayJoin(...names.map((name) => [groupName, name])))
    ));
    return reduceToCsv([headers, ...data], ";");
}
