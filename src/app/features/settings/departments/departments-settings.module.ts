/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CdkTableModule} from "@angular/cdk/table";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatIconModule} from "@angular/material/icon";
import {TranslateModule} from "@ngx-translate/core";
import {ActionButtonModule} from "../../../shared/layout/action-button";
import {CollapsibleModule} from "../../../shared/layout/collapsible";
import {ListModule} from "../../../shared/layout/list";
import {PaginationCounterModule} from "../../../shared/layout/pagination-counter";
import {SearchbarModule} from "../../../shared/layout/searchbar";
import {SideMenuModule} from "../../../shared/layout/side-menu";
import {SharedPipesModule} from "../../../shared/pipes";
import {SharedSettingsModule} from "../shared";
import {DepartmentsSettingsComponent, DepartmentsSettingsSearchComponent, DepartmentsSettingsTableComponent} from "./components";
import {SectorsFromDepartmentPipe} from "./pipes";

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        CdkTableModule,
        MatIconModule,

        ActionButtonModule,
        CollapsibleModule,
        ListModule,
        PaginationCounterModule,
        SearchbarModule,
        SharedPipesModule,
        SharedSettingsModule,
        SideMenuModule
    ],
    declarations: [
        DepartmentsSettingsTableComponent,
        DepartmentsSettingsSearchComponent,
        DepartmentsSettingsComponent,

        SectorsFromDepartmentPipe
    ],
    exports: [
        DepartmentsSettingsTableComponent,
        DepartmentsSettingsSearchComponent,
        DepartmentsSettingsComponent,

        SectorsFromDepartmentPipe
    ]
})
export class DepartmentsSettingsModule {

}
