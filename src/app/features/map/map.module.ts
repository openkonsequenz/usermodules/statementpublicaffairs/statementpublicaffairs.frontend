/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {TranslateModule} from "@ngx-translate/core";
import {APP_CONFIGURATION, IAppConfiguration} from "../../core/configuration";
import {ActionButtonModule} from "../../shared/layout/action-button";
import {SearchbarModule} from "../../shared/layout/searchbar";
import {SideMenuRegistrationService} from "../../shared/layout/side-menu/services";
import {ProgressSpinnerModule} from "../../shared/progress-spinner";
import {LeafletMapComponent} from "./components/leaflet";
import {MapSelectComponent} from "./components/map-select";
import {LeafletDirective, LeafletMarkerDirective, LeafletPopupDirective} from "./directives";
import {LEAFLET_CONFIGURATION_TOKEN, LEAFLET_RESIZE_TOKEN} from "./leaflet-configuration.token";
import {StringToLatLngZoomPipe} from "./pipes";

@NgModule({
    imports: [
        CommonModule,
        ActionButtonModule,
        TranslateModule,
        SearchbarModule,
        ProgressSpinnerModule,
        MatButtonModule,
        MatIconModule
    ],
    declarations: [
        LeafletDirective,
        LeafletMarkerDirective,
        LeafletPopupDirective,
        LeafletMapComponent,
        StringToLatLngZoomPipe,
        MapSelectComponent
    ],
    exports: [
        LeafletDirective,
        LeafletMarkerDirective,
        LeafletPopupDirective,
        LeafletMapComponent,
        StringToLatLngZoomPipe,
        MapSelectComponent
    ],
    providers: [
        {
            provide: LEAFLET_CONFIGURATION_TOKEN,
            useFactory: (config: IAppConfiguration) => config.leaflet,
            deps: [APP_CONFIGURATION]
        },
        {
            provide: LEAFLET_RESIZE_TOKEN,
            useFactory: (service: SideMenuRegistrationService) => service.resize$,
            deps: [SideMenuRegistrationService]
        }
    ]
})
export class MapModule {

}
