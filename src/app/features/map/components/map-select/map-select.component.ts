/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, forwardRef, Input, Output} from "@angular/core";
import {NG_VALUE_ACCESSOR} from "@angular/forms";
import {LeafletMouseEvent} from "leaflet";
import {AbstractControlValueAccessorDirective} from "../../../../shared/controls/common";
import {ILeafletBounds} from "../../directives/leaflet";
import {latLngZoomToString} from "../../util";

@Component({
    selector: "app-map-select",
    templateUrl: "./map-select.component.html",
    styleUrls: ["./map-select.component.scss"],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => MapSelectComponent),
            multi: true
        }
    ]
})
export class MapSelectComponent extends AbstractControlValueAccessorDirective<string> {

    @Input()
    public appCenter: string;

    @Input()
    public appSubCaption: string;

    @Output()
    public appOpenGis = new EventEmitter<ILeafletBounds>();

    public select(clickEvent: LeafletMouseEvent, zoom: number) {
        const position = latLngZoomToString(clickEvent.latlng, zoom);
        if (position) {
            this.writeValue(position, true);
        }
    }

}
