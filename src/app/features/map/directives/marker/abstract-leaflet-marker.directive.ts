/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Directive, Inject, NgZone, OnDestroy, Output} from "@angular/core";
import {DivIcon, LatLngLiteral, LeafletEvent, LeafletMouseEvent, Marker} from "leaflet";
import {defer, Observable, of, pipe} from "rxjs";
import {switchMap} from "rxjs/operators";
import {runInZone, runOutsideZone} from "../../../../util/rxjs";
import {ILeafletConfiguration, LEAFLET_CONFIGURATION_TOKEN} from "../../leaflet-configuration.token";
import {fromLeafletEvent} from "../../util";
import {LeafletHandler} from "../leaflet";


@Directive({})
export abstract class AbstractLeafletMarkerDirective implements OnDestroy {

    @Output()
    public appClick = defer(() => this.on<LeafletMouseEvent>("click"));

    public readonly marker: Marker = new Marker(this.configuration, {
        icon: new DivIcon({className: "openk-leaflet-marker", iconSize: [30, 30]})
    });

    public constructor(
        public readonly ngZone: NgZone,
        public readonly leafletHandler: LeafletHandler,
        @Inject(LEAFLET_CONFIGURATION_TOKEN) public readonly configuration: ILeafletConfiguration,
    ) {

    }

    public ngOnDestroy() {
        this.remove();
    }

    public on<T extends LeafletEvent>(type: string, outsideZone?: boolean): Observable<T> {
        return of(type).pipe(
            runOutsideZone(this.ngZone),
            switchMap((_) => fromLeafletEvent<T>(this.marker, type)),
            outsideZone ? pipe() : runInZone(this.ngZone)
        );
    }

    protected setLatLng(value?: LatLngLiteral): boolean {
        return this.ngZone.runOutsideAngular(() => {
            if (value != null && Number.isFinite(value.lat) && Number.isFinite(value.lng)) {
                this.marker.setLatLng(value);
                this.marker.addTo(this.leafletHandler.instance);
                return true;
            }
            return false;
        });
    }

    protected remove() {
        this.ngZone.runOutsideAngular(() => this.marker.remove());
    }

}
