/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CdkDragDrop} from "@angular/cdk/drag-drop";
import {Component, Input} from "@angular/core";
import {FormGroup} from "@angular/forms";
import {IAPITextArrangementItemModel, IAPITextBlockGroupModel} from "../../../../../core";
import {IExtendedTextBlockModel} from "../../../../../shared/text-block/model";
import {createStatementEditorForm, IStatementEditorControlConfiguration} from "../../../../../store";
import {AbstractReactiveFormArrayDirective} from "../../../abstract";

@Component({
    selector: "app-arrangement-form-group",
    templateUrl: "./arrangement-form-group.component.html",
    styleUrls: ["./arrangement-form-group.component.scss"]
})
export class ArrangementFormGroupComponent extends AbstractReactiveFormArrayDirective<IAPITextArrangementItemModel> {

    @Input()
    public appFormGroup: FormGroup = createStatementEditorForm();

    @Input()
    public appControls: IStatementEditorControlConfiguration[];

    @Input()
    public appSelectedTextBlockIds: string[];

    @Input()
    public appTextBlockGroups: IAPITextBlockGroupModel[];

    @Input()
    public appShortMode: boolean;

    @Input()
    public appShowPreview: boolean;

    @Input()
    public appReplacements: { [key: string]: string };

    public appFormArrayName = "arrangement";

    public isDragging = false;

    public trackBy = (index: number) => {
        const control = this.getFormArrayControl(index);
        return control == null ? index : control;
        // tslint:disable-next-line:semicolon
    };

    public drop(event: CdkDragDrop<any>) {
        if (event.previousContainer !== event.container) {
            this.addTextBlock(event.item?.data, event.currentIndex);
        }
        if (event.previousContainer === event.container) {
            this.moveControl(event.previousIndex, event.currentIndex);
        }
    }

    public addTextBlock(block: IExtendedTextBlockModel, index?: number) {
        if (block == null) {
            return;
        }

        const {type, id} = block;
        if (type == null || type === "block") {
            if (id != null) {
                this.addControl({type: "block", textblockId: id, placeholderValues: {}}, index);
            }
        } else {
            this.addControl({type, placeholderValues: {}, replacement: type === "text" ? "" : undefined}, index);
        }
    }

}




