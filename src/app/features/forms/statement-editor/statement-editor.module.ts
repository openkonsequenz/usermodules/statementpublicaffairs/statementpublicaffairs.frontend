/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {DragDropModule} from "@angular/cdk/drag-drop";
import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MatSidenavModule} from "@angular/material/sidenav";
import {TranslateModule} from "@ngx-translate/core";
import {ClaimDetailsModule} from "../../../shared/controls/claim-details";
import {SelectModule} from "../../../shared/controls/select";
import {FilePreviewModule} from "../../../shared/file-preview";
import {ActionButtonModule} from "../../../shared/layout/action-button";
import {CollapsibleModule} from "../../../shared/layout/collapsible";
import {GlobalClassToggleModule} from "../../../shared/layout/global-class-toggle";
import {SideMenuModule} from "../../../shared/layout/side-menu";
import {SharedPipesModule} from "../../../shared/pipes";
import {TextBlockModule} from "../../../shared/text-block";
import {CombineBlockdataTextPipe} from "../../../shared/text-block/pipes/combine-blockdata-text/combine-blockdata-text.pipe";
import {StatementDetailsModule} from "../../details";
import {AttachmentsFormModule} from "../attachments";
import {CommentsFormModule} from "../comments";
import {
    ArrangementFormGroupComponent,
    StatementEditorFormComponent,
    StatementEditorSideMenuComponent,
    StatementPreviewComponent,
    TextBlockControlComponent
} from "./components";
import {ArrangementToPreviewPipe, ErrorToMessagesPipe, GetTitlePipe} from "./pipes";


@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        SharedPipesModule,
        DragDropModule,
        TextBlockModule,
        CollapsibleModule,
        MatIconModule,
        GlobalClassToggleModule,
        MatSidenavModule,
        AttachmentsFormModule,
        SideMenuModule,
        ActionButtonModule,
        TranslateModule,
        FilePreviewModule,
        SelectModule,
        StatementDetailsModule,
        CommentsFormModule,
        ClaimDetailsModule
    ],
    declarations: [
        StatementEditorFormComponent,
        TextBlockControlComponent,
        GetTitlePipe,
        ErrorToMessagesPipe,
        StatementPreviewComponent,
        ArrangementToPreviewPipe,
        CombineBlockdataTextPipe,
        ArrangementFormGroupComponent,
        CombineBlockdataTextPipe,
        StatementEditorSideMenuComponent
    ],
    exports: [
        StatementEditorFormComponent,
        StatementPreviewComponent,
        ArrangementToPreviewPipe,
        ArrangementFormGroupComponent,
        StatementEditorSideMenuComponent
    ]
})
export class StatementEditorModule {

}
