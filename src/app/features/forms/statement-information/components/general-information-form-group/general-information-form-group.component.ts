/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from "@angular/core";
import {FormGroup} from "@angular/forms";
import {IAPISectorsModel} from "../../../../../core";
import {ISelectOption} from "../../../../../shared/controls/select";
import {createStatementInformationForm} from "../../../../../store";

@Component({
    selector: "app-general-information-form-group",
    templateUrl: "./general-information-form-group.component.html",
    styleUrls: ["./general-information-form-group.component.scss"]
})
export class GeneralInformationFormGroupComponent {

    private static id = 0;

    public appId = `GeneralInfoFormGroupComponent${GeneralInformationFormGroupComponent.id++}`;

    @Input()
    public appStatementTypeOptions: ISelectOption[] = [];

    @Input()
    public appFormGroup: FormGroup = createStatementInformationForm();

    @Input()
    public appSectors: IAPISectorsModel = {};

    protected get disabled() {
        return !this.appStatementTypeOptions || this.appStatementTypeOptions.length === 0;
    }
}
