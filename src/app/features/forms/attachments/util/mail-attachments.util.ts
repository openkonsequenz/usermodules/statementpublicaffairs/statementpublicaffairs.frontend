/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIAttachmentModel} from "../../../../core/api/attachments";
import {IAPIEmailAttachmentModel} from "../../../../core/api/mail";
import {IAttachmentControlValue} from "../../../../store/attachments/model";
import {arrayJoin} from "../../../../util/store";

/**
 * Given the list of statement attachments and the list of the statement attachments of an email,
 * this function returns a list of the attachment objects to the email attachment with tag "email".
 * isSelected is set if email attachment is already in the list of statement attachments or for a new statement. (default value)
 */
export function getMailAttachments(
    statementAttachments: IAttachmentControlValue[], emailAttachments: IAPIEmailAttachmentModel[], isNewStatement: boolean = false) {
    return arrayJoin(emailAttachments).map((emailAttachment) => {
        const emailAttachmentFromAttachmentsArray = arrayJoin(statementAttachments).find((attachment) =>
            attachment.name === emailAttachment.name && attachment.tagIds.find((_) => _ === "email") != null);

        const isSelected = (emailAttachmentFromAttachmentsArray != null) || isNewStatement;

        const tagIds = emailAttachmentFromAttachmentsArray?.tagIds;

        return {
            ...(emailAttachmentFromAttachmentsArray ? emailAttachmentFromAttachmentsArray : emailAttachment),
            isSelected,
            tagIds: arrayJoin(tagIds)
        };
    });
}

/**
 * Returns the email text attachment from the list of attachments.
 */
export function getMailAttachment(attachments: IAPIAttachmentModel[]): IAPIAttachmentModel {
    return arrayJoin(attachments).find((_) =>
        _.name === "mailText.txt" &&
        _.tagIds.length === 2 &&
        _.tagIds.find((tagId) => tagId === "email-text") != null &&
        _.tagIds.find((tagId) => tagId === "email") != null
    );
}
