/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IAPIAttachmentTag} from "../../../../core/api/attachments";

@Pipe({
    name: "appFilterOutTags"
})
export class FilterOutTagsPipe implements PipeTransform {

    public transform(tags: IAPIAttachmentTag[], tagIds: string[]) {
        return tags.filter((tag) => tagIds.find((t) => tag.id === t) == null);
    }

}
