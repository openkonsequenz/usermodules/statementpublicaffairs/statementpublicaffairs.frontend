/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {IAPIAttachmentModel} from "../../../../core/api/attachments";
import {IAPIEmailModel} from "../../../../core/api/mail";
import {GetEmailTextAttachmentPipe} from "./get-email-text-attachment.pipe";

describe("GetEmailTextAttachmentPipe", () => {

    const pipe = new GetEmailTextAttachmentPipe();

    describe("transform", () => {

        it("should return the mail text attachment with name set to mail subject", () => {

            const mail: IAPIEmailModel = {
                subject: "subject",
                from: "from"
            } as IAPIEmailModel;
            const attachments: IAPIAttachmentModel[] = [
                {name: "attachment1", tagIds: []},
                {name: "attachment2", tagIds: []},
                {name: "mailText.txt", tagIds: ["email", "email-text"]}
            ] as IAPIAttachmentModel[];

            const mailTextAttachment = pipe.transform(attachments, mail);
            expect(mailTextAttachment).toEqual({...attachments[2], name: `"${mail.subject}" (${mail.from})`});

        });

        it("should return null if no mail text attachment was found", () => {
            let mailTextAttachment = pipe.transform(null, null);
            expect(mailTextAttachment).toEqual(null);

            mailTextAttachment = pipe.transform([], null);
            expect(mailTextAttachment).toEqual(null);
        });
    });
});

