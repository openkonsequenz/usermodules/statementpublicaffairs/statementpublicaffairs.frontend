/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Pipe, PipeTransform} from "@angular/core";
import {IAPIAttachmentModel} from "../../../../core/api/attachments";
import {IAPIEmailModel} from "../../../../core/api/mail";
import {getMailAttachment} from "../util/mail-attachments.util";

@Pipe({
    name: "appGetEmailTextAttachment"
})
export class GetEmailTextAttachmentPipe implements PipeTransform {


    /**
     * Given the mail data and attachments list, gets the mail text attachment and replaces the name with the mail subject.
     */

    public transform(attachments: IAPIAttachmentModel[], mail: IAPIEmailModel): IAPIAttachmentModel {
        const mailTextAttachment = getMailAttachment(attachments);

        if (!mailTextAttachment) {
            return null;
        }
        const name = mail ? `"${mail.subject}" (${mail.from})` : mailTextAttachment.name;
        return {
            ...mailTextAttachment,
            name
        };
    }

}
