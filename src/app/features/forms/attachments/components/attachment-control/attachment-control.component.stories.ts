/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {boolean, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../../core/i18n";
import {createAttachmentTagList} from "../../../../../test";
import {AttachmentsFormModule} from "../../attachments-form.module";

storiesOf("Features / Forms / Attachments", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        imports: [
            I18nModule,
            AttachmentsFormModule
        ]
    }))
    .add("AttachmentControlComponent", () => ({
        template: `
            <app-attachment-control style="margin: 1em; box-sizing: border-box"
                [appValue]="appValue"
                [appTagList]="appTagList"
                [appIsCancelable]="forManualFileUpload()"
                [appIsSelectable]="!forManualFileUpload()"
                [appIsDownloadable]="!forManualFileUpload()">
            </app-attachment-control>
        `,
        props: {
            appValue: {name: "Anhang.pdf", tagIds: []},
            appTagList: createAttachmentTagList(),
            forManualFileUpload: () => boolean("forManualFileUpload", false)
        }
    }));
