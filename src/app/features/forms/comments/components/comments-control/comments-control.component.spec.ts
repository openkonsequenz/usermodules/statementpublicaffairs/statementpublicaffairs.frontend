/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, ElementRef, SimpleChange, ViewChild} from "@angular/core";
import {ComponentFixture, TestBed} from "@angular/core/testing";
import {By} from "@angular/platform-browser";
import {IAPICommentModel} from "../../../../../core/api/statements";
import {I18nModule} from "../../../../../core/i18n";
import {CommentsFormModule} from "../../comments-form.module";
import {CommentsControlComponent} from "./comments-control.component";

@Component({
    selector: "app-host-component",
    template: `
        <app-comments-control
            #comments
            [appComments]="appComments"
            [appCommentsToShow]="appCommentsToShow"
            (appDelete)="deleteComment($event)"
            (appAdd)="addComment($event)">
        </app-comments-control>
    `
})
class TestHostComponent {

    public appComments: Array<IAPICommentModel>;

    @ViewChild("comments", {read: ElementRef}) comments: ElementRef;

    public appCommentsToShow = 3;

    public constructor() {
        this.appComments = [];
    }

    public addComment(text: string) {
        this.appComments.push(
            {
                id: this.appComments.length,
                text,
                userName: "test01",
                firstName: "Vorname",
                lastName: "Nachname",
                timestamp: new Date().toString(),
                editable: true
            }
        );
    }

    public deleteComment(id: number) {
        this.appComments.splice(id - 1, 1);
    }
}

describe("CommentsControlComponent", () => {
    let component: TestHostComponent;
    let fixture: ComponentFixture<TestHostComponent>;
    let childComponent: CommentsControlComponent;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [
                TestHostComponent
            ],
            imports: [
                CommentsFormModule,
                I18nModule
            ]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(TestHostComponent);
        component = fixture.componentInstance;
        childComponent = fixture.debugElement.query(By.directive(CommentsControlComponent)).componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should emit appNewComment with the comment text", () => {
        spyOn(childComponent.appAdd, "emit").and.callThrough();
        childComponent.textValue = "test comment text";
        childComponent.onSave();
        expect(childComponent.appAdd.emit).toHaveBeenCalledWith("test comment text");
    });

    it("should emit appDeleteComment with the comment id", () => {
        spyOn(childComponent.appDelete, "emit").and.callThrough();
        childComponent.onDelete(1);
        expect(childComponent.appDelete.emit).toHaveBeenCalledWith(1);
    });

    it("should always show the full textarea", () => {
        const textField = fixture.debugElement.query(By.css(".comments--newcomment--textfield")).nativeElement;
        expect(textField.scrollTop).toBe(0);
        textField.value = `A long comment that wraps to multiple rows.
         There should be no scroll bar, the textarea field has to grow with the given input. So scrolltop has to stay 0.`;
        textField.dispatchEvent(new Event("input"));
        fixture.detectChanges();
        expect(textField.scrollTop).toBe(0);
    });

    it("should reset value on save and delete", () => {
        childComponent.textValue = "Example value";
        childComponent.onSave();
        expect(childComponent.textValue).toEqual("");
        childComponent.textValue = "Example value";
        childComponent.clear();
        expect(childComponent.textValue).toEqual("");
    });

    it("should only show save button when there was a text input", () => {
        let button = fixture.debugElement.query(By.css(".openk-button.openk-success"));
        expect(button).toBeFalsy();
        childComponent.textAreaRef.nativeElement.value = "value";
        fixture.detectChanges();
        button = fixture.debugElement.query(By.css(".openk-button.openk-success"));
        expect(button).toBeTruthy();
    });

    it("should show commentsToShow amount of comments", () => {
        childComponent.appComments = new Array(22).fill(1).map((el, index) => ({
            id: index,
            text: "test text",
            userName: "User1",
            firstName: "Franz",
            lastName: "Meier",
            timestamp: "2007-08-31T16:47+00:00",
            editable: true
        }));
        childComponent.appCommentsToShow = 5;
        fixture.detectChanges();
        let comments = fixture.debugElement.queryAll(By.css(".comments--list--comment"));
        expect(comments.length).toEqual(5);
        childComponent.showMore();
        fixture.detectChanges();
        comments = fixture.debugElement.queryAll(By.css(".comments--list--comment"));
        expect(comments.length).toEqual(10);
        childComponent.showMore(true);
        fixture.detectChanges();
        comments = fixture.debugElement.queryAll(By.css(".comments--list--comment"));
        expect(comments.length).toEqual(22);
    });

    it("should end current editing on reload", () => {
        spyOn(childComponent, "endEdit");
        childComponent.ngOnChanges({});
        expect(childComponent.endEdit).not.toHaveBeenCalled();
        childComponent.ngOnChanges({appComments: {} as SimpleChange});
        expect(childComponent.endEdit).toHaveBeenCalled();
    });

    it("should set the id for the selected comment", () => {
        childComponent.selectedCommentIndex = null;
        childComponent.onEdit(5);
        expect(childComponent.selectedCommentIndex).toBe(5);
        childComponent.onEdit(3);
        expect(childComponent.selectedCommentIndex).toBe(3);
    });

    it("should send edit comment request only if text changed", () => {
        spyOn(childComponent, "endEdit");
        spyOn(childComponent.appEdit, "emit");

        childComponent.editTextValue = "";
        childComponent.onSendEdit(5);
        expect(childComponent.endEdit).toHaveBeenCalled();
        expect(childComponent.appEdit.emit).not.toHaveBeenCalled();

        childComponent.editTextValue = "changedText";
        childComponent.onSendEdit(5);
        expect(childComponent.endEdit).toHaveBeenCalled();
        expect(childComponent.appEdit.emit).toHaveBeenCalledWith({id: 5, newText: "changedText"});
    });
});
