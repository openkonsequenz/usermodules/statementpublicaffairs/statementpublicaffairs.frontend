/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, ElementRef, EventEmitter, Input, OnChanges, Output, SimpleChanges, ViewChild} from "@angular/core";
import {IAPICommentModel} from "../../../../../core";
import {momentFormatDisplayFullDateAndTime} from "../../../../../util";

@Component({
    selector: "app-comments-control",
    templateUrl: "./comments-control.component.html",
    styleUrls: ["./comments-control.component.scss"]
})
export class CommentsControlComponent implements OnChanges {

    @Input()
    public appCollapsed: boolean;

    @Input()
    public appCommentsToShow = 5;

    @Input()
    public appComments: Array<IAPICommentModel>;

    @Output()
    public appDelete: EventEmitter<number> = new EventEmitter();

    @Output()
    public appEdit: EventEmitter<{ id: number; newText: string }> = new EventEmitter();

    @Output()
    public appAdd: EventEmitter<string> = new EventEmitter();

    @Input()
    public timeDisplayFormat: string = momentFormatDisplayFullDateAndTime;

    @Output()
    public appCommentsToShowChange = new EventEmitter<number>();

    public textValue = "";

    public editTextValue = "";

    public selectedCommentIndex: number;

    @ViewChild("textAreaElement")
    public textAreaRef: ElementRef<HTMLTextAreaElement>;

    public ngOnChanges(changes: SimpleChanges) {
        if (changes.appComments) {
            this.endEdit();
        }
    }

    public onSave() {
        this.appAdd.emit(this.textValue);
        this.clear();
    }

    public onSendEdit(commentId: number) {
        if (this.editTextValue !== "") {
            this.appEdit.emit({id: commentId, newText: this.editTextValue});
        }
        this.endEdit();
    }

    public onDelete(id: number) {
        this.appDelete.emit(id);
    }

    public onEdit(id: number) {
        this.selectedCommentIndex = id;
    }

    public clear() {
        this.textValue = "";
    }

    public endEdit() {
        this.selectedCommentIndex = null;
        this.editTextValue = "";
    }

    public showMore(all = false) {
        if (this.appCommentsToShow == null) {
            this.appCommentsToShow = 0;
        }
        this.appCommentsToShow = all ? undefined : Math.min(this.appComments?.length, this.appCommentsToShow + 5);
        this.appCommentsToShowChange.emit(this.appCommentsToShow);
    }

}
