/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
    selector: "app-date-filter",
    templateUrl: "./date-filter.component.html",
    styleUrls: ["./date-filter.component.scss"]
})
export class DateFilterComponent {

    @Input()
    public appActive: boolean;

    @Input()
    public appTitle: string;

    @Output()
    public appValueChange = new EventEmitter<Date>();

    public emitNewValue(value: Date) {
        if (this.appActive) {
            this.appValueChange.emit(value);
        }
    }

}
