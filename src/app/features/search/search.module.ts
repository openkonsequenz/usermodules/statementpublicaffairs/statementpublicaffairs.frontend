/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {MatIconModule} from "@angular/material/icon";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {DateControlModule} from "../../shared/controls/date-control";
import {SelectModule} from "../../shared/controls/select";
import {PaginationCounterModule} from "../../shared/layout/pagination-counter";
import {SearchbarModule} from "../../shared/layout/searchbar";
import {StatementTableModule} from "../../shared/layout/statement-table";
import {MapModule} from "../map";
import {DateFilterComponent} from "./components/date-filter";
import {PositionSearchComponent} from "./components/position-search";
import {SearchFilterComponent} from "./components/search-filter";
import {SearchStatementsComponent} from "./components/search-statements";

@NgModule({
    imports: [
        CommonModule,
        StatementTableModule,
        PaginationCounterModule,
        SearchbarModule,
        SelectModule,
        DateControlModule,
        TranslateModule,
        MatIconModule,
        RouterModule,
        MapModule
    ],
    declarations: [
        SearchFilterComponent,
        DateFilterComponent,
        SearchStatementsComponent,
        PositionSearchComponent
    ],
    exports: [
        SearchFilterComponent,
        DateFilterComponent,
        SearchStatementsComponent,
        PositionSearchComponent
    ]
})
export class SearchModule {

}
