/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export enum EStatementEditSites {

    LOADING = "loading",

    STATEMENT_INFORMATION_FORM = "statementInformationForm",

    WORKFLOW_DATA_FORM = "workflowDataForm",

    STATEMENT_EDITOR_FORM = "statementEditorForm",

    DRAFT_FOR_NEGATIVE_ANSWER_FORM = "draftForNegativeAnswerForm",

}
