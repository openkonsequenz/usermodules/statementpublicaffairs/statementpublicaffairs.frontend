/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {TranslateModule} from "@ngx-translate/core";
import {SharedPipesModule} from "../../shared/pipes";
import {ProgressSpinnerModule} from "../../shared/progress-spinner";
import {CommentsFormModule} from "../forms/comments";
import {StatementEditorModule} from "../forms/statement-editor";
import {StatementInformationFormModule} from "../forms/statement-information";
import {WorkflowDataFormModule} from "../forms/workflow-data/workflow-data-form.module";
import {EditDebugComponent, StatementEditPortalComponent} from "./components";

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,

        ProgressSpinnerModule,
        CommentsFormModule,
        StatementInformationFormModule,
        WorkflowDataFormModule,
        SharedPipesModule,
        StatementEditorModule
    ],
    declarations: [
        StatementEditPortalComponent,
        EditDebugComponent
    ]
})
export class StatementEditModule {

}
