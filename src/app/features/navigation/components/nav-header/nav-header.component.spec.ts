/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {RouterTestingModule} from "@angular/router/testing";
import {EAPIUserRoles, I18nModule} from "../../../../core";
import {TestCoreModule} from "../../../../core/test-core.module";
import {AppNavigationFrameModule} from "../../app-navigation-frame.module";
import {NavHeaderComponent} from "./nav-header.component";

describe("NavHeaderComponent", () => {
    let component: NavHeaderComponent;
    let fixture: ComponentFixture<NavHeaderComponent>;

    beforeEach(waitForAsync(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                AppNavigationFrameModule,
                I18nModule,
                TestCoreModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(NavHeaderComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should check if link is active", () => {
        expect(component.isLinkActive("/new")).toBe(false);
        component.appCurrentUrl = "/new";
        expect(component.isLinkActive("/new")).toBe(true);
        expect(component.isLinkActive("/")).toBe(true);
        expect(component.isLinkActive("/", true)).toBe(false);
    });

    it("should check if link is displayed", () => {
        expect(component.isLinkDisplayed(null)).toBe(true);
        expect(component.isLinkDisplayed([])).toBe(true);
        component.appUserRoles = [EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE];
        expect(component.isLinkDisplayed([])).toBe(true);
        expect(component.isLinkDisplayed([EAPIUserRoles.SPA_APPROVER])).toBe(false);
        expect(component.isLinkDisplayed([EAPIUserRoles.SPA_OFFICIAL_IN_CHARGE])).toBe(true);
    });

});

