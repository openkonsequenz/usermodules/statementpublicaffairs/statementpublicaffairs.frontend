/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {MatIconModule} from "@angular/material/icon";
import {action} from "@storybook/addon-actions";
import {select, text, withKnobs} from "@storybook/addon-knobs";
import {moduleMetadata, storiesOf} from "@storybook/angular";
import {I18nModule} from "../../../../core";
import {EExitCode} from "../../../../store";
import {ExitPageComponent} from "./exit-page.component";

storiesOf("Features / 01 Navigation", module)
    .addDecorator(withKnobs)
    .addDecorator(moduleMetadata({
        declarations: [
            ExitPageComponent
        ],
        imports: [
            I18nModule,
            MatIconModule
        ]
    }))
    .add("ExitPageComponent", () => ({
        component: ExitPageComponent,
        props: {
            appExitCode: select("Exit Code", EExitCode, EExitCode.LOGOUT),
            appHref: text("Link", "/"),
            appRouting: action("appRouting")
        }
    }));
