/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component} from "@angular/core";
import {ComponentFixture, TestBed, waitForAsync} from "@angular/core/testing";
import {I18nModule} from "../../../../core";
import {EExitCode} from "../../../../store";
import {AppNavigationFrameModule} from "../../app-navigation-frame.module";
import {ExitPageComponent} from "./exit-page.component";

describe("ExitPageComponent", () => {

    describe("basic component", () => {
        let component: ExitPageComponent;
        let fixture: ComponentFixture<ExitPageComponent>;

        beforeEach(waitForAsync(() => {
            TestBed.configureTestingModule({
                imports: [I18nModule, AppNavigationFrameModule],
                declarations: [ExitPageComponent]
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ExitPageComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
        });

        it("should create", () => {
            expect(component).toBeTruthy();
        });
    });

    describe("outputs", () => {

        let component: TestHostComponent;
        let fixture: ComponentFixture<TestHostComponent>;

        beforeEach(waitForAsync(() => {
            TestBed.configureTestingModule({
                imports: [I18nModule, AppNavigationFrameModule],
                declarations: [ExitPageComponent, TestHostComponent],
                providers: []
            })
                .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(TestHostComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
        });

        it("should emit appRouting on button click", () => {
            spyOn(component, "onEmitAppRouting");
            component.appExitCode = EExitCode.FORBIDDEN;
            fixture.detectChanges();

            fixture.nativeElement.querySelector("button").dispatchEvent(new Event("click"));
            expect(component.onEmitAppRouting).toHaveBeenCalled();
        });

        it("should emit appClosing on button click", () => {
            spyOn(component, "onEmitAppClosing");
            component.appExitCode = EExitCode.LOGOUT;
            fixture.detectChanges();

            fixture.nativeElement.querySelector("a").dispatchEvent(new Event("click"));
            expect(component.onEmitAppClosing).toHaveBeenCalled();
        });

        @Component({
            selector: "app-host-component",
            template: `
                <app-exit-page [appExitCode]="appExitCode" [appHref]="appHref"
                               (appRouting)="onEmitAppRouting()"
                               (appClosing)="onEmitAppClosing()"></app-exit-page>`
        })
        class TestHostComponent {
            public appExitCode: EExitCode;
            public appHref: string;

            public onEmitAppRouting() {
            }

            public onEmitAppClosing() {
            }
        }
    });
});
