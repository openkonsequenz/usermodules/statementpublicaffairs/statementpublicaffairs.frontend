/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {ComponentFixture, TestBed} from "@angular/core/testing";
import {By} from "@angular/platform-browser";
import {I18nModule} from "../../../../core";
import {AppNavigationFrameModule} from "../../app-navigation-frame.module";
import {NavDropDownComponent} from "./nav-drop-down.component";

describe("NavDropDownComponent", () => {
    let component: NavDropDownComponent;
    let fixture: ComponentFixture<NavDropDownComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [AppNavigationFrameModule, I18nModule]
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(NavDropDownComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it("should create", () => {
        expect(component).toBeTruthy();
    });

    it("should emit log-out", async () => {
        let count = 0;
        const subscription = component.appLogOut.subscribe(() => count++);
        component.logOut();
        fixture.detectChanges();
        subscription.unsubscribe();
        expect(count).toEqual(1);
    });

    it("should show logout button after opening the dropdown", () => {
        let appDropDownButtons = fixture.debugElement.queryAll(By.css(".nav-drop-down-button"));
        expect(appDropDownButtons.length).toBe(1);

        fixture.nativeElement.querySelector(".nav-drop-down-button").click();
        fixture.detectChanges();

        appDropDownButtons = fixture.debugElement.queryAll(By.css(".nav-drop-down-button"));
        expect(appDropDownButtons.length).toBeGreaterThan(1);
    });

    it("should call logout function when pressing on the logout button", () => {
        spyOn(component, "logOut");

        const openDropDownButton = fixture.nativeElement.querySelector(".nav-drop-down-button");
        openDropDownButton.click();
        fixture.detectChanges();

        const logoutButton = fixture.debugElement.query(By.css(".nav-drop-down-menu-item")).nativeElement;
        logoutButton.click();

        expect(component.logOut).toHaveBeenCalled();
    });
});
