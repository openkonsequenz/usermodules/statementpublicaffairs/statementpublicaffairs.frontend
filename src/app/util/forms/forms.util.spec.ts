/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {shrinkString} from "./forms.util";

describe("FormsUtil", () => {

    it("shrinkString", () => {
        expect(shrinkString("  A B C ")).toEqual("A B C");
        expect(shrinkString("  ")).not.toBeDefined();
        expect(shrinkString(null)).not.toBeDefined();
        expect(shrinkString(undefined)).not.toBeDefined();

        expect(shrinkString("  A B C ", "default")).toEqual("A B C");
        expect(shrinkString("  ", "default")).toEqual("default");
        expect(shrinkString(null, "default")).toEqual("default");
        expect(shrinkString(undefined, "default")).toEqual("default");
    });

});
