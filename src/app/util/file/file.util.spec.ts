/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {parseCsv, reduceToCsv} from "./file.util";

describe("FileUtil", () => {
    const text = "1;2;3;4\r\n5;6;7;\r\n8;9;;";

    const data = [
        ["1", "2", "3", "4"],
        ["5", "6", "7"],
        ["8", "9"]
    ];

    it("parseCsv", () => {
        const expectedData = [data[0], [...data[1], ""], [...data[2], "", ""]];
        expect(parseCsv(text, ";")).toEqual(expectedData);
    });

    it("reduceToCsv", () => {
        expect(reduceToCsv(data, ";")).toEqual(text);
    });

});
