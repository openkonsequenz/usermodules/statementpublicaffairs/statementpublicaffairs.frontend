/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import * as moment from "moment";
import {momentDiff, momentFormatDisplayNumeric, momentFormatInternal, parseMomentToDate, parseMomentToString} from "./moment.util";

describe("MomentUtil", () => {

    it("parseMomentToDate", () => {
        const moments = [
            "01.05.2020",
            moment("2020-05-01", momentFormatInternal),
            new Date(2020, 4, 1)
        ];

        moments.forEach((m) => {
            const date = parseMomentToDate(m, momentFormatDisplayNumeric, null);

            expect(date instanceof Date).toBeTrue();
            expect(date.getFullYear()).toBe(2020);
            expect(date.getMonth()).toBe(4);
            expect(date.getDate()).toBe(1);
        });

        const defaultDate = new Date(2020, 4, 1);
        expect(parseMomentToDate(1588180997731, momentFormatInternal)).not.toBeDefined();
        expect(parseMomentToDate(1588180997731, momentFormatInternal, defaultDate)).toBe(defaultDate);
    });

    it("parseMomentToString", () => {
        const moments = [
            "01.05.2020",
            moment("2020-05-01", momentFormatInternal),
            new Date(2020, 4, 1)
        ];

        moments.forEach((m) => {
            expect(parseMomentToString(m, momentFormatDisplayNumeric, momentFormatInternal)).toBe("2020-05-01");
        });

        expect(parseMomentToString(1588180997731, momentFormatDisplayNumeric, momentFormatInternal)).toBe("");
    });

    it("momentDiff", () => {
        expect(momentDiff("2019-09-19", "2019-09-21")).toBe(-2 * 1000 * 60 * 60 * 24);
    });

});

