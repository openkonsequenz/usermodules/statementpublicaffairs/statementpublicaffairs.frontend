/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {HttpErrorResponse, HttpHeaders, HttpResponse} from "@angular/common/http";
import {defer, Observable, of, throwError} from "rxjs";
import {EHttpStatusCodes} from "./EHttpStatusCodes";
import {catchHttpError, catchHttpErrorTo, isHttpErrorWithStatus, mapHttpResponseToFile, objectToHttpParams, urlJoin} from "./http.util";

describe("HttpUtil", () => {

    it("urlJoin", () => {
        expect(urlJoin("ab", "cd")).toBe("ab/cd");
        expect(urlJoin("/ab", "///", "cd", "///ef")).toBe("/ab/cd/ef");
        expect(urlJoin("https://", "/////", "www.abc.de", "///", "/fg/")).toBe("https://www.abc.de/fg/");
        expect(urlJoin()).toBe("");
    });

    it("objectToHttpParams", () => {
        expect(objectToHttpParams(undefined)).toEqual({});
        expect(objectToHttpParams(null)).toEqual({});
        expect(objectToHttpParams({a: "1", b: 9, c: ["19", 1919]})).toEqual({a: "1", b: "9", c: ["19", "1919"]});
    });

    it("isHttpErrorWithStatus", () => {
        let httpError: any;

        httpError = new HttpErrorResponse({status: EHttpStatusCodes.UNAUTHORIZED});
        expect(isHttpErrorWithStatus(httpError, EHttpStatusCodes.UNAUTHORIZED)).toBeTrue();
        expect(isHttpErrorWithStatus(httpError, EHttpStatusCodes.UNAUTHORIZED, EHttpStatusCodes.FORBIDDEN)).toBeTrue();

        httpError = {status: EHttpStatusCodes.UNAUTHORIZED};
        expect(isHttpErrorWithStatus(httpError, EHttpStatusCodes.UNAUTHORIZED)).toBeFalse();
        expect(isHttpErrorWithStatus(httpError, EHttpStatusCodes.UNAUTHORIZED, EHttpStatusCodes.FORBIDDEN)).toBeFalse();

        httpError = new HttpErrorResponse({status: EHttpStatusCodes.FORBIDDEN});
        expect(isHttpErrorWithStatus(httpError, EHttpStatusCodes.UNAUTHORIZED)).toBeFalse();
        expect(isHttpErrorWithStatus(httpError, EHttpStatusCodes.UNAUTHORIZED, EHttpStatusCodes.FORBIDDEN)).toBeTrue();
    });

    it("catchHttpError", async () => {
        const httpError = new HttpErrorResponse({status: EHttpStatusCodes.UNAUTHORIZED});

        const noError$: Observable<any> = of(19);
        const error$: Observable<any> = throwError(httpError);

        let promise: Promise<any>;

        promise = noError$.pipe(catchHttpError(async () => -19)).toPromise();
        await expectAsync(promise).toBeResolvedTo(19);

        promise = error$.pipe(catchHttpError(async () => -19)).toPromise();
        await expectAsync(promise).toBeResolvedTo(-19);

        promise = error$.pipe(catchHttpError(async () => -19, httpError.status)).toPromise();
        await expectAsync(promise).toBeResolvedTo(-19);

        promise = error$.pipe(catchHttpError(async () => -19, 500, 501, httpError.status, 502)).toPromise();
        await expectAsync(promise).toBeResolvedTo(-19);

        promise = error$.pipe(catchHttpError(async () => -19, 500, 501, 502)).toPromise();
        await expectAsync(promise).toBeRejectedWith(httpError);
    });

    it("catchHttpErrorTo", async () => {
        const httpError = new HttpErrorResponse({status: EHttpStatusCodes.UNAUTHORIZED});
        const error$: Observable<any> = throwError(httpError);

        await expectAsync(error$.pipe(catchHttpErrorTo(19)).toPromise())
            .toBeResolvedTo(19);
        await expectAsync(error$.pipe(catchHttpErrorTo(19, EHttpStatusCodes.UNAUTHORIZED)).toPromise())
            .toBeResolvedTo(19);
        await expectAsync(error$.pipe(catchHttpErrorTo(19, EHttpStatusCodes.NOT_FOUND)).toPromise())
            .toBeRejectedWith(httpError);
    });

    it("mapHttpResponseToFile", async () => {
        const fileName = "" + Math.floor(Math.random() * 1000) + ".txt";
        const headers = new HttpHeaders({
            "content-disposition": `attachment; filename="${fileName}"`
        });
        const file = new File(["19"], "test.pdf");
        const file$ = defer(() => of(httpResponse)).pipe(mapHttpResponseToFile());
        let httpResponse: HttpResponse<Blob> = null;

        await expectAsync(file$.toPromise()).toBeRejected();

        httpResponse = new HttpResponse({body: null});
        await expectAsync(file$.toPromise()).toBeRejected();

        httpResponse = new HttpResponse({body: file, status: 200, headers});
        const result = await file$.toPromise();
        expect(result).toBeInstanceOf(File);
        expect(result.name).toBe(fileName);
        await expectAsync(result.text()).toBeResolvedTo("19");
    });

});

