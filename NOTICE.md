# Notices for Eclipse openK User Modules

This content is produced and maintained by the Eclipse openK User Modules project.

 * Project home: https://projects.eclipse.org/projects/technology.openk-usermodules

## Trademarks

Eclipse openK User Modules is a trademark of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the
listed source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/openk-usermodules
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.elogbook
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.elogbookFE
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.gridFailureInformation.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.gridFailureInformation.documentation
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.gridFailureInformation.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.mics.centralService
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.mics.homeService
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.documentation
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.docu
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.documentation
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.statementPublicAffairs.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.statementPublicAffairs.documentation
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.statementPublicAffairs.frontend

## Third-party Content

@angular-devkit/build-angular (0.901.12)
 * License: MIT
 * Homepage: https://github.com/angular/angular-cli

@angular/animations (9.1.12)
 * License: MIT
 * Homepage: https://github.com/angular/angular#readme

@angular/cdk (9.2.4)
 * License: MIT
 * Homepage: https://github.com/angular/components#readme

@angular/common (9.1.12)
 * License: MIT
 * Homepage: https://github.com/angular/angular#readme

@angular/core (9.1.12)
 * License: MIT
 * Homepage: https://github.com/angular/angular#readme

@angular/forms (9.1.12)
 * License: MIT
 * Homepage: https://github.com/angular/angular#readme

@angular/material (9.2.4)
 * License: MIT
 * Homepage: https://github.com/angular/components#readme

@angular/platform-browser (9.1.12)
 * License: MIT
 * Homepage: https://github.com/angular/angular#readme
 
@angular/platform-browser-dynamic (9.1.12)
 * License: MIT
 * Homepage: https://github.com/angular/angular#readme

@angular/router (9.1.12)
 * License: MIT
 * Homepage: https://github.com/angular/angular/tree/master/packages/router

@ngrx/effects (9.2.0)
 * License: MIT
 * Homepage: https://github.com/ngrx/platform#readme

@ngrx/store (9.2.0)
 * License: MIT
 * Homepage: https://github.com/ngrx/platform#readme

@ngx-translate/core (12.1.2)
 * License: MIT
 * Homepage: https://github.com/ngx-translate/core

@ngx-translate/http-loader (5.0.0)
 * License: MIT
 * Homepage: https://github.com/ngx-translate/http-loader

bpmn-js (6.5.1)
 * License: bpmn.io License (https://bpmn.io/license)
 * Homepage: https://github.com/bpmn-io/bpmn-js#readme

core-js (3.6.4)
 * License: MIT
 * Homepage: https://github.com/zloirock/core-js#readme

css-loader (3.5.1)
 * License: MIT
 * Homepage: https://github.com/webpack-contrib/css-loader

leaflet (1.6.0)
 * License: BSD-2-Clause
 * Homepage: https://leafletjs.com/

material-design-icons (3.0.1)
 * License: Apache-2.0
 * Homepage: https://github.com/google/material-design-icons

moment (2.27.0)
 * License: MIT
 * Homepage: https://momentjs.com

primeng (9.1.3)
 * License: MIT
 * Homepage: https://github.com/primefaces/primeng#readme

regenerator-runtime (0.13.5)
 * License: MIT
 * Repository: https://github.com/facebook/regenerator/tree/master/packages/regenerator-runtime

rxjs (6.5.5)
 * License: Apache-2.0
 * Homepage: https://github.com/ReactiveX/RxJS

source-sans-pro (3.6.0)
 * License: OFL-1.1
 * Homepage: https://adobe-fonts.github.io/source-sans-pro

tslib (1.13.0)
 * License: 0BSD
 * Homepage: https://www.typescriptlang.org/

webpack (4.42.0)
 * License: MIT
 * Homepage: https://github.com/webpack/webpack

zone.js (0.10.3)
 * License: MIT
 * Homepage: https://github.com/angular/angular#readme

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
