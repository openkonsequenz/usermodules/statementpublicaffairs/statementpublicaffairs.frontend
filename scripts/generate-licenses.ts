/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

// This script generates a listing of all packages and their licenses used for production (dev dependencies are omitted).

const path = require("path");
const licenseChecker = require("license-checker");
const fs = require("fs");
const packageJson = require("../package.json");

const PATH_TO_PACKAGE_JSON = path.join("./");
const PATH_FOR_LICENSES_FILE = path.join(__dirname, "..", "licenses.txt");

async function checkLicenses(pathToPackageJson: string, options: any = {production: true}): Promise<any> {
    return new Promise<any>((resolve, reject) => {
        licenseChecker.init({...options, start: pathToPackageJson}, (error, data) => error != null ? reject(error) : resolve(data));
    });
}

async function main() {
    let result = "";

    const data = await checkLicenses(PATH_TO_PACKAGE_JSON);

    Object.keys(data)
        .map<any | { name: string; version: string; }>((key) => {
            const obj = data[key];

            const lastIndexOfAt = key.lastIndexOf("@");
            const name = key.substr(0, lastIndexOfAt);
            const version = key.substr(lastIndexOfAt + 1);

            return {...obj, name, version};
        })
        // Omit all results which are not listed in the package.json as dependency:
        .filter((packageModule) => packageJson.dependencies[packageModule.name] != null)
        .forEach((packageModule) => {
            result += packageModule.name + " (" + packageModule.version + ")" + "\n";
            result += " * License: " + packageModule.licenses + "\n";
            result += " * Source: " + packageModule.repository + "\n\n";
        });

    console.log(result);
    fs.writeFileSync(PATH_FOR_LICENSES_FILE, result);
}

main();
